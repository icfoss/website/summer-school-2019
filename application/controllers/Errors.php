<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends CI_Controller {


  public function index()
  {
    $data['head_name'] = "ICFOSS-Schools-error";
    $this->load->view('site/head.php',$data);
    $this->load->view('site/nav.php');
    $this->load->view('site/404.php');
    $this->load->view('site/footer.php');
    $this->load->view('site/htmlend.php');

  }
  
}
