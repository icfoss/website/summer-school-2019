<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	function __construct() {
         parent::__construct();


     }

	public function index()
	{

		$this->load->model('Home_core','',TRUE);
		$data['query']=$this->Home_core->get_all_config();
		$data['query2']=$this->Home_core->get_all_partners();
		$data['query3']=$this->Home_core->get_allspeakers();
    $data['head_name'] = "ICFOSS-Schools-home";
		$data['page']=1;
    $this->load->view('site/head.php',$data);
    $this->load->view('site/nav.php',$data);
		$this->load->view('site/home.php',$data);
    $this->load->view('site/footer.php');
    $this->load->view('site/htmlend.php');
	}
	public function about()
	{
			$data['page']=2;
		$this->load->model('Home_core','',TRUE);
		$data['query']=$this->Home_core->get_all_config();
		$data['head_name'] = "ICFOSS-Schools-about us";
		$this->load->view('site/head.php',$data);
		$this->load->view('site/nav.php',$data);
		$this->load->view('site/about.php');
		$this->load->view('site/footer.php');
		$this->load->view('site/htmlend.php');
	}
	public function wabout()
	{
		$data['page']=2;
		$this->load->model('Home_core','',TRUE);
		$data['query']=$this->Home_core->get_all_config();
		$data['head_name'] = "ICFOSS-about winter-school";
		$this->load->view('site/head.php',$data);
		$this->load->view('site/nav.php',$data);
		$this->load->view('site/wabout.php');
		$this->load->view('site/footer.php');
		$this->load->view('site/htmlend.php');
	}
	public function sabout()
	{
			$data['page']=2;
		$this->load->model('Home_core','',TRUE);
		$data['query']=$this->Home_core->get_all_config();
		$data['head_name'] = "ICFOSS-about-summer-school";
		$this->load->view('site/head.php',$data);
		$this->load->view('site/nav.php',$data);
		$this->load->view('site/sabout.php');
		$this->load->view('site/footer.php');
		$this->load->view('site/htmlend.php');
	}
	public function registration()
	{
			$data['page']=3;
		$this->load->model('Home_core','',TRUE);
		$data['query']=$this->Home_core->get_all_config();
		$data['head_name'] = "ICFOSS-Schools-registration";
		$this->load->view('site/head.php',$data);
		$this->load->view('site/nav.php',$data);
		$this->load->view('site/registration.php',$data);
		$this->load->view('site/footer.php');
		$this->load->view('site/htmlend.php');
	}
	public function actionRegister()
 {
	 $result="fail";
	 $status="fail";
	 $this->load->model('Register_core','',TRUE);
	 $this->load->library('form_validation');
	 $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[4]');
	 $this->form_validation->set_rules('email', 'Your Email', 'trim|required|valid_email');
   $this->form_validation->set_rules('phone','Phone no','trim|required');
	 $this->form_validation->set_rules('address','Address','trim|required');
	 $this->form_validation->set_rules('category','	Category','trim|required');
	 $this->form_validation->set_rules('area','Area of Interest','trim|required');
	 $this->form_validation->set_rules('moto','Reason for choosing','trim|required');
	 $this->form_validation->set_rules('qualification','qualification','trim|required');
	 if($this->form_validation->run() == FALSE) {
		 $this->registration();
	 } else {
		 // post values
		 $name = $this->input->post('name');
		 if( is_dir("uploads/register/".$name) === false )
{
    mkdir("uploads/register/".$name,0755);
	//	shell_exec("chown -R www-data:www-data uploads/register/".$name );
}

		 $config['upload_path']    = 'uploads/register/'.$name;
                $config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx|odt';
                $config['max_size']             = 20000;
                $config['max_width']            = 600;
                $config['max_height']           = 600;
                $config['overwrite']=TRUE;
			 		      $this->load->library('upload', $config);
                $this->upload->initialize($config);

		 $titile=$this->input->post('titile');
		 $name = $this->input->post('name');
		 $address = $this->input->post('address');
		 $email = $this->input->post('email');
		 $phone = $this->input->post('phone');
		 $qualification=$this->input->post('qualification');
		 $category=$this->input->post('category');
		 $area=$this->input->post('area');
		 $moto=$this->input->post('moto');
		 if($this->upload->do_upload('iotstudy'))
		 {
		 $casestudy = $this->upload->data('file_name');
    // $casestudy="case_study_".$name."_".$casestudy;

		 }
		 else {
		 	 $casestudy ="not uploaded";
		 }


if($this->upload->do_upload('photo'))
{
 $photo = $this->upload->data('file_name');
// $photo=$name."_".$photo;
}
else {
	 $photo ="not uploaded";
}

if($this->upload->do_upload('bio'))
{
$biodata = $this->upload->data('file_name');
//$biodata="biodata_".$name."_".$biodata;
}
else {
	 $biodata ="not uploaded";
}

if(!isset($casestudy))
{
$statusMsg = "Error in Iot casestudy file upload..";
$this->session->set_flashdata('statusMsg',$statusMsg);
$this->registration();

}else if(!isset($photo))
{
$statusMsg = "Error in photo   upload..";
$this->session->set_flashdata('statusMsg',$statusMsg);
$this->registration();
}else if(!isset($biodata))
{
$statusMsg = "Error in biodata file   upload..";
$this->session->set_flashdata('statusMsg',$statusMsg);
$this->registration();
}
else
 {


$data=array(
    "fullname"=> $titile.".".$name,
		"address"=>$address,
		"mobile_no"=>$phone,
	  "email"=>$email,
		"edu"=>$qualification,
		"cate"=>$category,
		"area"=>$area,
		"reason"=>$moto,
		 "photo"=>$photo,
		 "profile"=>$biodata,
		 "casestudy"=>$casestudy
);
$result=$this->Register_core->add_register($data);
}
if($result=='ok')
{
	$satus1=$this->send_email("akshai.m@icfoss.in",$data);
	$status=$this->send($email,($titile.".".$name));
}
if($status=="ok")
{

	$statusMsg = 'Registration Successful..';
	$this->session->set_flashdata('statusMsg',$statusMsg);
	$this->registration();
}
else {

	$this->session->set_flashdata('statusMsg',$status);
	$this->registration();
}
	 }
 }

	public function selected()
	{
		$data['page']=3;
		$this->load->model('Home_core','',TRUE);
		$data['query']=$this->Home_core->get_all_config();
		$data['query1']=$this->Home_core->selection();
		$data['query2']=$this->Home_core->waiting();
		$data['head_name'] = "ICFOSS-Schools-Selected";
		$this->load->view('site/head.php',$data);
		$this->load->view('site/nav.php',$data);
		$this->load->view('site/list.php',$data);
		$this->load->view('site/footer.php');
		$this->load->view('site/htmlend.php');
	}
	public function Schedule()
	{
			$data['page']=4;
		$this->load->model('Home_core','',TRUE);
		$data['query']=$this->Home_core->get_all_config();
		$data['head_name'] = "ICFOSS-Schools-Schedule";
		$this->load->view('site/head.php',$data);
		$this->load->view('site/nav.php',$data);
		$this->load->view('site/schedule.php');
		$this->load->view('site/footer.php');
		$this->load->view('site/htmlend.php');
	}
	public function speaker()
	{
			$data['page']=5;
		$this->load->model('Home_core','',TRUE);
		$data['query']=$this->Home_core->get_all_config();
		$data['query2']=$this->Home_core->get_allspeakers();
		$data['head_name'] = "ICFOSS-Schools-Speaker";
		$this->load->view('site/head.php',$data);
		$this->load->view('site/nav.php',$data);
		$this->load->view('site/speaker.php',$data);
		$this->load->view('site/footer.php');
		$this->load->view('site/htmlend.php');
	}
	public function profile()
	{ $data['page']=5;
		$this->load->model('Home_core','',TRUE);
		$sid=$this->input->get('sp_id');
		$data['query']=$this->Home_core->get_all_config();
		$data['query3']=$this->Home_core->get_onespeaker($sid);
		$data['head_name'] = "ICFOSS-Schools-Speaker-profile";
		$this->load->view('site/head.php',$data);
		$this->load->view('site/nav.php',$data);
		$this->load->view('site/profile.php',$data);
		$this->load->view('site/footer.php');
		$this->load->view('site/htmlend.php');
	}
	public function gallery()
	{
		$this->load->model('Home_core','',TRUE);
		$this->load->model('Gallery_core','',TRUE);
		$data['page']=6;
		$data['query']=$this->Home_core->get_all_config();
		$data['query1']=$this->Gallery_core->get_current_year();
		if (isset($data['query1']))
		{
		       foreach( $data['query1'] as $row3)
		            {

		              $current_year=$row3->year;


		            }

		}

		$data['query2']=$this->Gallery_core->get_all_year();
		$data['query3']=$this->Gallery_core->get_all_img($current_year);
		$data['head_name'] = "ICFOSS-Schools-gallery";
		$this->load->view('site/head.php',$data);
		$this->load->view('site/nav.php',$data);
		$this->load->view('site/gallery.php',$data);
		$this->load->view('site/footer.php');
		$this->load->view('site/htmlend.php');
	}
	public function download()
	{ 	$data['page']=7;
		$this->load->model('Home_core','',TRUE);
		$data['query']=$this->Home_core->get_all_config();
		$data['head_name'] = "ICFOSS-Schools-download";
		$this->load->view('site/head.php',$data);
		$this->load->view('site/nav.php',$data);
		$this->load->view('site/download.php');
		$this->load->view('site/footer.php');
		$this->load->view('site/htmlend.php');
	}
	public function committee()
	{ 	$data['page']=8;
		$this->load->model('Home_core','',TRUE);
		$this->load->helper('form');
		$data['query']=$this->Home_core->get_all_config();
		$data['head_name'] = "ICFOSS-Schools-contact";
		$this->load->view('site/head.php',$data);
		$this->load->view('site/nav.php',$data);
		$this->load->view('site/committee.php');
		$this->load->view('site/footer.php');
		$this->load->view('site/htmlend.php');

	}
	public function contact()
	{ $data['page']=9;
		$this->load->model('Home_core','',TRUE);
		$this->load->helper('form');
		$data['query']=$this->Home_core->get_all_config();
		$data['head_name'] = "ICFOSS-Schools-contact";
		$this->load->view('site/head.php',$data);
		$this->load->view('site/nav.php',$data);
		$this->load->view('site/contact.php');
		$this->load->view('site/footer.php');
		$this->load->view('site/htmlend.php');
	}
	public function actioncontact()
	{

		$this->load->model('Contact_core','',TRUE);
			$this->load->helper('form');
	  $this->load->library('form_validation');
	  $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[4]');
	  $this->form_validation->set_rules('email', 'Email','trim|required');
		$this->form_validation->set_rules('subject', 'Subject', 'trim|required');
		$this->form_validation->set_rules('message', 'Message', 'trim|required');
	  if($this->form_validation->run() == FALSE) {
	 	 $this->contact();
	  } else {
		   	$name=$this->input->post('name');
				$email=$this->input->post('email');
				$subject=$this->input->post('subject');
				$message=$this->input->post('message');
			   $data = array(
	             'cont_name' =>$name,
	             'cont_email' =>$email,
	             'cont_subject'=>$subject,
					     'cont_message'=>$message
	       );
				 $result=$this->Contact_core->add_contact($data);
				 if($result=='ok')
				 {
					 $statusMsg = 'Thank you for contacting us..!,we will get back you soon.';
 					$this->session->set_flashdata('statusMsg',$statusMsg);
 					$this->contact();

				 }
	 }



	}


	public function send_email($email,$data)
	{

		$this->load->library('PHPmailer_lib');

		$mail = $this->phpmailer_lib->load();

try {


//Server settings
$mail->CharSet = 'UTF-8';
$mail->SMTPDebug = 0;                                 // Enable verbose debug output
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';   // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'noreply@icfoss.in';                 // SMTP username
$mail->Password = 'n0r3ply@G2!';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

//Recipients


$mail->setFrom('noreply@icfoss.in', 'Noreply');
$mail->addAddress($email,$data['fullname']);     // Add a recipient
//Content
$mail->AddCC("schools@icfoss.org");
$mail->isHTML(true);                                  // Set email format to HTML
$mail->Subject = 'Registeration for Summer school 2019';
$message = $this->load->view('site/email_html1', $data, true);
$mail->Body= $message;
$mail->send();
return 'ok';
}
catch (Exception $e) {
echo 'Message could not be sent.';
echo 'Mailer Error: ' . $mail->ErrorInfo;
}

	 }
	function send($email,$name){
	$this->load->library('PHPmailer_lib');

	$mail = $this->phpmailer_lib->load();
        $this->load->model('Home_core','',TRUE);

        $mail_data['query']=$this->Home_core->get_all_config();

try {


//Server settings
$mail->CharSet = 'UTF-8';
$mail->SMTPDebug = 0;                                 // Enable verbose debug output
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';   // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'noreply@icfoss.in';                 // SMTP username
$mail->Password = 'n0r3ply@G2!';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

//Recipients


$mail->setFrom('noreply@icfoss.in', 'Noreply');
$mail->addAddress($email,$name);     // Add a recipient
//Content
$mail->isHTML(true);                               // Set email format to HTML
$mail->Subject = 'Registration for Summer school 2019';
$mail_data['name'] = $name;
$message = $this->load->view('site/email_html', $mail_data, true);
$mail->Body= $message;
$mail->send();
return 'ok';
}
catch (Exception $e) {
echo 'Message could not be sent.';
echo 'Mailer Error: ' . $mail->ErrorInfo;
}

 }
}
