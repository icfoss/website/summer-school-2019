<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

        function __construct() {
             parent::__construct();

	        if ($this->session->userdata('icfoss_login_user') == null || $this->session->userdata('icfoss_login_user') < 1) {
	            // Prevent infinite loop by checking that this isn't the login controller
	            if ($this->router->class != 'login')
	            {
	                redirect(site_url().'login');
	            }
	        }

         }

	public function index()
	{
        $data['page']=1;
        $this->load->view('admin/header.php');
        $this->load->view('admin/navbar.php',$data);
        $this->load->view('admin/dash.php');
        $this->load->view('admin/footer.php');
        $this->load->view('admin/htmlend.php');
    }
    public function register()
	{
        $data['page']=2;
        $this->load->model('Admin_core','',TRUE);
		$data['query']=$this->Admin_core->registerselect();
        $this->load->view('admin/header.php');
        $this->load->view('admin/navbar.php',$data);
        $this->load->view('admin/registered.php',$data);
        $this->load->view('admin/footer.php');
        $this->load->view('admin/htmlend.php');
    }
    public function selected()
	{
        $data['page']=3;
        $this->load->model('Admin_core','',TRUE);
        $data['query']=$this->Admin_core-> selectionlist();
        $this->load->view('admin/header.php');
        $this->load->view('admin/navbar.php',$data);
        $this->load->view('admin/selected.php',$data);
        $this->load->view('admin/footer.php');
        $this->load->view('admin/htmlend.php');
    }
    public function rejected()
	{
        $data['page']=3;
        $this->load->model('Admin_core','',TRUE);
        $data['query']=$this->Admin_core-> rejectionlist();
        $this->load->view('admin/header.php');
        $this->load->view('admin/navbar.php',$data);
        $this->load->view('admin/rejection.php',$data);
        $this->load->view('admin/footer.php');
        $this->load->view('admin/htmlend.php');
    }
    public function waiting()
	{
        $data['page']=3;
        $this->load->model('Admin_core','',TRUE);
        $data['query']=$this->Admin_core-> waitinglist();
        $this->load->view('admin/header.php');
        $this->load->view('admin/navbar.php',$data);
        $this->load->view('admin/waiting.php',$data);
        $this->load->view('admin/footer.php');
        $this->load->view('admin/htmlend.php');
    }
    public function view()
    {
        $this->load->model('Admin_core','',TRUE);
        $rid=$this->input->get('rid');
        $data['page']=2;
        if(isset($rid)){
            $data['query1']=$this->Admin_core->viewuser($rid);
            $this->load->view('admin/header.php');
            $this->load->view('admin/navbar.php',$data);
            $this->load->view('admin/profile.php',$data);
            $this->load->view('admin/footer.php');
            $this->load->view('admin/htmlend.php');

        }
        else
        {
            redirect(site_url().'admin/register');
        }
    }
    public function rremove()
    {
        $this->load->model('Admin_core','',TRUE);
        $rid=$this->input->get('rid');
        $data['page']=2;
        if(isset($rid)){
            $result=$this->Admin_core->removeuser($rid);
            if( $result=='ok')
            {
                $this->session->set_flashdata('statusMsg',"deleted successfully..");
                $this->register();
            }
            else
            {
                $this->session->set_flashdata('statusMsg',"deletion failed..");
                $this->register();
            }
        }
        else{

            redirect(site_url().'admin/register');
        }

    }

  public function listremove()
  {
//echo "hii";
     $this->load->model('Admin_core','',TRUE);
     $rid=$this->input->get('l_rid');
     if(isset($rid)){
         $result=$this->Admin_core->lremove($rid);
         if( $result=='ok')
         {
             $this->session->set_flashdata('statusMsg',"Added to rejected list..");
             $this->rejected();
         }
         else
         {
             $this->session->set_flashdata('statusMsg',"operation failed..");
             $this->register();
         }
     }
     else{

        redirect(site_url().'admin/rejected');
     }

  }







  public function selectupdate()
  {
      $this->load->model('Admin_core','',TRUE);
      $rid=$this->input->get('rid');
      $ltype=$this->input->get('ltype');
      $email=$this->input->get('email');
      $name=$this->input->get('name');
      // $data['page']=2;
      if(isset($rid)){
        $result=$this->Admin_core->listupdate($rid,$ltype);
        if( $result=='ok')
        {  if($ltype==1)
          {
            $status=$this->send($email,$name);
            $this->session->set_flashdata('statusMsg',"Added to the Selection list");
          }
          else if($ltype==2)
          {
            $this->session->set_flashdata('statusMsg',"Added to the Waiting list");
          }
          else if($ltype==3)
          {
            $this->session->set_flashdata('statusMsg',"Added to the Rejection list");
          }
            $this->view();
        }
        else
        {
            $this->session->set_flashdata('statusMsg',"failed..");
            $this->view();

        }

  }
  else {
    {
redirect(site_url().'admin/register');
    }
  }
}
public function partners()
{
    $data['page']=4;
    $this->load->model('Admin_core','',TRUE);
    $data['query']=$this->Admin_core-> getpartners();
    $this->load->view('admin/header.php');
    $this->load->view('admin/navbar.php',$data);
    $this->load->view('admin/partners.php',$data);
    $this->load->view('admin/footer.php');
    $this->load->view('admin/htmlend.php');
}

public function premove()
 {
   $this->load->model('Admin_core','',TRUE);
   $pid=$this->input->get('pt_id');
   if(isset($pid)){
       $result=$this->Admin_core->ptremove($pid);
       if( $result=='ok')
       {
           $this->session->set_flashdata('statusMsg',"Added to rejected list..");
           $this->partners();
       }
       else
       {
           $this->session->set_flashdata('statusMsg',"operation failed..");
           $this->partners();
       }
   }
   else{

      redirect(site_url().'admin/partners');
   }

 }

 public function actionPartner()
{
  $this->load->model('Admin_core','',TRUE);
  $this->load->library('form_validation');
  $this->form_validation->set_rules('pname', 'name ', 'trim|required|min_length[4]');
  //$this->form_validation->set_rules('logo','image','trim|required');
  if($this->form_validation->run() == FALSE) {
      $this->partners();
  }
  else{
    $config['upload_path']= 'uploads/brand/';
               $config['allowed_types']        = 'gif|jpg|png|svg|jpeg';
               $config['max_size']             = 10024;
               $config['overwrite']=TRUE;
               $this->load->library('upload', $config);
               $this->upload->initialize($config);
  $name = $this->input->post('pname');
  if($this->upload->do_upload('logo'))
  {
     $logo= $this->upload->data('file_name');

  }
  else {


    $logo="error";
  }
  $data=array(
      "pt_name"=> $name,
  		"pt_image"=>$logo
    );
    $result=$this->Admin_core->add_partner($data);

    if($result=='ok')
    {
      $statusMsg = 'added..!';
       $this->session->set_flashdata('statusMsg',$statusMsg);
        $this->partners();

    }
    else{
      $statusMsg = 'error..!';
       $this->session->set_flashdata('statusMsg',$statusMsg);
        $this->partners();
    }
   }
  }




  public function speakers()
  {
      $data['page']=5;
      $this->load->model('Admin_core','',TRUE);
      $data['query']=$this->Admin_core->getspeakers();
      $this->load->view('admin/header.php');
      $this->load->view('admin/navbar.php',$data);
      $this->load->view('admin/speakerlist.php',$data);
      $this->load->view('admin/footer.php');
      $this->load->view('admin/htmlend.php');
  }
  public function addspeaker()
  {
      $data['page']=5;
      $this->load->model('Admin_core','',TRUE);
      //$data['query']=$this->Admin_core;
      $this->load->view('admin/header.php');
      $this->load->view('admin/navbar.php',$data);
      $this->load->view('admin/spadd.php');
      $this->load->view('admin/footer.php');
      $this->load->view('admin/htmlend.php');
  }
  public function action_speaker()
  {
    $this->load->model('Admin_core','',TRUE);
    $this->load->library('form_validation');
    $this->form_validation->set_rules('spname', 'name ', 'trim|required|min_length[4]');
    $this->form_validation->set_rules('des', 'designation', 'trim|required|min_length[3]');
    $this->form_validation->set_rules('org', 'Organisation', 'trim|required|min_length[4]');
    //$this->form_validation->set_rules('session', 'session', 'trim|required|min_length[]');
    $this->form_validation->set_rules('about', 'about', 'trim|required|min_length[4]');
    if($this->form_validation->run() == FALSE) {
    //  echo "fail";
      $this->speakers();
    }
    else{
      $config['upload_path']= 'uploads/speaker/';
                 $config['allowed_types']        = 'jpg|png|svg|jpeg';
                 $config['max_size']             = 10024;
                 $config['overwrite']=TRUE;
                 $this->load->library('upload', $config);
                 $this->upload->initialize($config);
    $name = $this->input->post('spname');
    $des = $this->input->post('des');
    $org = $this->input->post('org');
    $session = $this->input->post('session');
    $about = $this->input->post('about');
    if($this->upload->do_upload('img'))
    {
       $pic= $this->upload->data('file_name');
     }
    else {
       $pic="error";
    }

    $data=array(
        "sp_name"=>$name,
        "sp_des"=>$des,
        "sp_org"=>$org,
      //  "sp_session"=>$session,
        "sp_about"=>$about,
        "sp_image"=>$pic
      );

      $result=$this->Admin_core->add_speaker($data);

      if($result=='ok')
      {
        $statusMsg = 'added..!';
         $this->session->set_flashdata('statusMsg',$statusMsg);
          $this->speakers();

      }
      else{
        $statusMsg = 'error..!';
         $this->session->set_flashdata('statusMsg',$statusMsg);
          $this->speakers();
      }
     }


  }



  public function sp_remove()
   {
     $this->load->model('Admin_core','',TRUE);
     $sid=$this->input->get('sp_id');
     if(isset($sid)){
         $result=$this->Admin_core->sremove($sid);
         if( $result=='ok')
         {
             $this->session->set_flashdata('statusMsg',"deleted successfully...");
             $this->speakers();
         }
         else
         {
             $this->session->set_flashdata('statusMsg',"operation failed..");
             $this->speakers();
         }
     }
     else{

            redirect(site_url().'admin/speakers');
     }

   }



  public function speaker_pro()
  {
    $data['page']=5;
    $sid=$this->input->get('sp_id');
    $this->load->model('Admin_core','',TRUE);
    $data['query1']=$this->Admin_core-> getprofile($sid);
    $this->load->view('admin/header.php');
    $this->load->view('admin/navbar.php',$data);
    $this->load->view('admin/sprofile.php',$data);
    $this->load->view('admin/footer.php');
    $this->load->view('admin/htmlend.php');
  }

  function send($email,$name){
    //  print_r($email);
      $mail = $this->phpmailer_lib->load();

  try {


  //Server settings
  $mail->CharSet = 'UTF-8';
  $mail->SMTPDebug = 0;                                 // Enable verbose debug output
  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'smtp.gmail.com';   // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;                               // Enable SMTP authentication
  $mail->Username = 'noreply@icfoss.in';                 // SMTP username
  $mail->Password = 'n0r3ply@G2!';                           // SMTP password
  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = 587;                                    // TCP port to connect to

  //Recipients


  $mail->setFrom('noreply@icfoss.in', 'Noreply');
  $mail->addAddress($email,$name);     // Add a recipient
  //Content
  $mail->isHTML(true);                                  // Set email format to HTML
  $mail->Subject = 'Registration status Summer school 2019';
  $mail_data['name'] = $name;
  $message = $this->load->view('admin/email2_html', $mail_data, true);
  $mail->Body= $message;
  $mail->send();
  return 'ok';
  }
  catch (Exception $e) {
  echo 'Message could not be sent.';
  echo 'Mailer Error: ' . $mail->ErrorInfo;
  }

     }




 public function edit_view()
 {

    $data['page']=7;
    $this->load->model('Admin_core','',TRUE);
    $sid=$this->input->get('sp_id');
    $data['query']=$this->Admin_core->getonesp($sid);
    $this->load->view('admin/header.php');
    $this->load->view('admin/navbar.php',$data);
    $this->load->view('admin/edit.php',$data);
    $this->load->view('admin/footer.php');
    $this->load->view('admin/htmlend.php');


   }

     public function sp_edit()
       {
         $this->load->model('Admin_core','',TRUE);
         $sname=$this->input->post('sname');
          $sess=$this->input->post('session');
          $abt=$this->input->post('about');
         // echo $sess;
          $data['query3']=$this->Admin_core-> s_edit($sname,$sess,$abt);
           if(isset($sid)){
             $result=$this->Admin_core->s_edit($sid,$sess,$abt);
          if( $result=='ok')
          {
              $this->session->set_flashdata('statusMsg',"updated successfully...");
              $this->speakers();
          }
          else
          {
              $this->session->set_flashdata('statusMsg',"operation failed..");
              $this->speakers();
          }
      }
      else{

             redirect(site_url().'admin/speakers');
      }

    }
}
