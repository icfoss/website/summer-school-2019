<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
 
    public function _construct()
    {

        parent::_construct();
      
       


    }
	
	public function index()
	{
        $this->load->model('Install_core','',TRUE);
        $result=$this->Install_core->isInstall();
        if($result===0)
        {
            redirect('/install');
        }
        else{
       
	
        $this->load->view('admin/login');
        }

    }
    public function loginmodule()
    {
        $this->load->model('Login_core','',TRUE);
        $result=$this->Login_core->checkuser();
        if($result=="ok")
        {
            echo json_encode(array('status'=>'ok'));
        }
        else{
            echo json_encode(array('status'=>$result));
        }
    }
    
        public function logoutuser()
        {
            $user=$this->session->userdata('icfoss_login_user');
            if(isset($user))
            {
              $sess_array = array(
                  'icfoss_login_user' => NULL         
                   );
                $this->session->set_userdata($sess_array);
               redirect(site_url().'login');
            }
            else{

            redirect(site_url().'login');
            }
        }
    

   
}
