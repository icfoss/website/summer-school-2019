<?php
	class Gallery_core extends CI_Model{
		public function _construct()
		{

			parent::_construct();


		}
		public function get_all_img($year)
	    {

      $this->db->where('year ',$year);
			$query = $this->db->get('front_gallery');
		    return $query->result();
	    }
    public function get_all_year()
    {
      $this->db->select('year');
      $this->db->distinct();
      $query = $this->db->get('front_gallery');
      return $query->result();
    }
    public function get_current_year()
    {
        $this->db->select_max('year');
        $query = $this->db->get('front_gallery');
        return $query->result();

    }


  }
?>
