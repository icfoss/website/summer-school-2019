<?php
	class Admin_core extends CI_Model{
		public function _construct()
		{

			parent::_construct();


		}
		function registerselect()
	    {
			$query = $this->db->get('front_registration');
		    return $query->result();
        }
        function viewuser($rid)
        {

            $this->db->where('rid',$rid);
		    	$query = $this->db->get('front_registration');
		    return $query->result();

        }
        function removeuser($rid)
        {

            $this->db->where('rid',$rid);
			$result = $this->db->delete('front_registration');

            if(isset($result))
            {
                return 'ok';
            }
            else
            {
                return 'fail';
            }

        }


    function listupdate($rid,$ltype)
		{
			$data = array(
        'l_rid' => $rid,
        'l_type' => $ltype

       );

    //  $s1=$this->db->insert('front_list', $data);
			$this->db->set('status', $ltype);
      $this->db->where('rid', $rid);
      $s2=$this->db->update("front_registration");

      if(isset($s2))
       {
				 return "ok";
			 }
			 else
			 	{
        return "fail";
				}

		}

    public function selectionlist(){


			   $this->db->where('status',1);
		     $query = $this->db->get('front_registration');
				 return $query->result();

      }
			public function waitinglist(){

				$this->db->where('status',2);
				$query = $this->db->get('front_registration');
				return $query->result();

				}
				public function rejectionlist(){

					$this->db->where('status',3);
					$query = $this->db->get('front_registration');
					return $query->result();

					}
		public function lremove($rid)
		{


			 $this->db->set('status',3);
			 $this->db->where('rid', $rid);
			 $s2=$this->db->update("front_registration");
			 if(isset($s2))
				{
				 return "ok";
			  }
			  else
				{
				  return "fail";
				}


		}



		public function getpartners()
		{
			$this->db->where('evnt_type',0);
			$query = $this->db->get('front_partners');
			return $query->result();
		}
		function ptremove($pid)
		{

				$this->db->where('pt_id',$pid);
	    $result = $this->db->delete('front_partners');

				if(isset($result))
				{
						return 'ok';
				}
				else
				{
						return 'fail';
				}

		}
		 function add_partner($data)
		 {
			$result = $this->db->insert('front_partners', $data);
			 if(isset($result))
			 {
					 return 'ok';
			 }
			 else
			 {
					 return 'fail';
			 }
		 }




		 public function getspeakers()
		 {
			 $this->db->where('evnt_type',0);
				$this->db->select('sp_id,sp_name, sp_des');
				$query = $this->db->get('front_speaker');
			 return $query->result();
		 }

		public function getprofile($sid)
		 {
				$this->db->where('sp_id', $sid);
				$query = $this->db->get('front_speaker');
				return $query->result();

			}
			function add_speaker($data)
			{
				$this->db->insert('front_speaker', $data);
				return "ok";
			}
			function sremove($sid)
			{

				$this->db->where('sp_id',$sid);
				$result = $this->db->delete('front_speaker');

					if(isset($result))
					{
							return 'ok';
					}
					else
					{
							return 'fail';
					}
	}

	function s_edit($sname,$sess,$abt)
	{

		$data = array(
     'sp_name' => $sname,
 		 'sp_session' => $sess,
		 'sp_about'=>$abt
 		);

		 $this->db->where('sp_name',$sname);
	 	$result = $this->db->update('front_speaker',$data);

			if(isset($result))
			{
					return 'ok';
			}
			else
			{
					return 'fail';
			}
}
public function getonesp($sid)
{
	$this->db->where('sp_id',$sid);
	 $this->db->select('sp_name');
	 $query = $this->db->get('front_speaker');
	return $query->result();
}

}
?>
