<?php
	class Install_core extends CI_Model{
		public function _construct()
		{

			parent::_construct();


		}
		function isInstall()
	    {
            if($this->db->table_exists('cms_config')==FALSE)
			{
				return 0;
			}
			else
			{
				return 1;
			}
        }
        function adddatabase()
	    {
            $this->db->query("CREATE TABLE cms_config (config_id int(20) NOT NULL,
                cevn_type int(4) NOT NULL DEFAULT '0',con_heading varchar(600) NOT NULL,
                con_active int(11) NOT NULL DEFAULT '0',
                con_logo1 varchar(100) NOT NULL,
                con_logo2 varchar(100) NOT NULL,
                con_about longtext NOT NULL,
                con_acrgss int(11) NOT NULL DEFAULT '0',
                con_speaker int(11) NOT NULL DEFAULT '0',
                con_schedule int(11) NOT NULL DEFAULT '0',
                con_partners int(11) NOT NULL DEFAULT '0',
                con_download int(11) NOT NULL DEFAULT '0',
                con_event int(100) NOT NULL DEFAULT '0',
                conp3 varchar(300) NOT NULL DEFAULT 'reserved',
                con_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
              )");

         return 1;
	    }


  }
?>
