

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                  Partners List
                </h1>

            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <!-- <h3 class="box-title">Partners List</h3> -->

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>

                        </div>
                    </div>

                    <div class="box-body">

              <table id="waiting" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Serail No:</th>
                  <th>Name</th>
                  <th>Logo</th>
                  <!-- <th style="width: 40px">View</th> -->
                  <th style="width: 40px">Remove</th>
                </tr>
                </thead>
                <tbody>
                  <?php if(isset($query))
                  {
                  ;
                  $c=1;

                  foreach ($query as $row1 ) {

                 $pid=$row1->pt_id;




                  ?>



                                  <tr>
                                    <td><?php echo $c;?></td>
                                    <td><?php echo $row1->pt_name;?></td>
                                    <td><img style="width:200px;height:100px;" src="<?php echo base_url();?>uploads/brand/<?php echo $row1->pt_image;?>" class="img-responsive"></td>
                                    <td><a href="<?php echo base_url(); ?>/admin/premove?pt_id=<?php echo $pid;?>"><button class="badge bg-red">Remove</button></a></td>
                                  </tr>

                  <?php

                  }}
                   ?>

                </tbody>
                <tfoot>
                <!-- <tr>
                <th>Serial No:</th>
                  <th>Name</th>
                  <th>E-mail</th>
                  <th style="width: 40px">View</th>
                  <th style="width: 40px">Remove</th>
                </tr> -->
                </tfoot>
              </table>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                      <div>
                       <button class="btn btn-primary" data-toggle="modal" data-target="#modal-info">Add</button>
                      </div>

                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
                <!-- /.modal -->

                        <div class="modal modal-info fade" id="modal-info">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Add New Partner</h4>
                              </div>
                              <div class="modal-body">
                              <table>
                               <thead>
                        </thead>
                  <?php echo form_open_multipart('admin/actionPartner'); ?>
                      <tbody class="table">
                         <tr>
                         <td>
                           <div>
                           <label class="control-label col-sm-3 col-md-3 text-center" for="name">Partner's Name:</label>
                                 <div class="col-sm-8 col-md-8">
                               <input name="pname" type="text" class="form-control " id="name" placeholder="enter the name">
                               </div>
                             </div>

                          </td>
                        </tr>
                        <tr>
                           <td>
                             <div>
                             <label class="control-label col-sm-3 col-md-3 text-center" for="name"> Logo:</label>
                             <div class="col-sm-8 col-md-8">
                                <input name="logo" class="input-file" id="my-file" type="file" value="">
                               </div>
                               </div>
                           </td>
                              </tr>
                                </tbody>
                                    </table>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                <button   name="submit" type="submit" class="btn btn-outline">Save</button>
                              </div>
                              <?php echo form_close(); ?>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <script>
   $(document).ready(function () {

    $('#waiting').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });
</script>
