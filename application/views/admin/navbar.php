<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
     <h3><i style="color:white;" class="glyphicon glyphicon-user" aria-hidden="true"></i></h3> 

      </div>
      <div class="pull-right info">
        <p>ADMIN</p>
      
      </div>
      <div class="logout">
      
        </div>
    </div>
    <!-- search form -->

    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
   

      <li <?php if(isset($page) && $page==1){ echo 'class=" active"'; }?> >
        <a href="<?php echo base_url(); ?>admin/">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          <span class="pull-right-container">
          <?php if(isset($page) && $page==1){ echo ' <i class="fa fa-angle-right pull-right"></i>'; }else{echo ' <i class="fa fa-angle-left pull-right"></i>';} ?>
           
          </span>
        </a>
    
      </li>
      <li <?php if(isset($page) && $page==2){ echo 'class=" active"'; }?>>
        <a href="<?php echo base_url(); ?>admin/register">
        <i class="fa fa-user-plus"></i> <span>Registration</span>
          <span class="pull-right-container">
          <?php if(isset($page) && $page==2){ echo ' <i class="fa fa-angle-right pull-right"></i>'; }else{echo ' <i class="fa fa-angle-left pull-right"></i>';} ?>
          </span>
        </a>
    
      </li>


      <li class="treeview  <?php if(isset($page) && $page==3){ echo 'class=" active"'; }?> ">
        <a href="#">
          <i class="fa fa-table"></i> <span>Participants</span>
          <span class="pull-right-container">
          <?php if(isset($page) && $page==3){ echo ' <i class="fa fa-angle-right pull-right"></i>'; }else{echo ' <i class="fa fa-angle-left pull-right"></i>';} ?>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url(); ?>admin/selected"><i class="fa fa-circle-o"></i> Selection List</a></li>
          <li ><a href="<?php echo base_url(); ?>admin/waiting"><i class="fa fa-circle-o"></i> Waiting List</a></li>
          <li><a href="<?php echo base_url(); ?>admin/rejected"><i class="fa fa-circle-o"></i> Rejection List</a></li>

        </ul>
      </li>
      <li class="treeview  <?php if(isset($page) && $page==5){ echo 'class="active"'; }?> ">
          <a href="#">
            <i class="fa fa-table"></i> <span>Speakers</span>
            <span class="pull-right-container">
            <?php if(isset($page) && $page==5){ echo ' <i class="fa fa-angle-right pull-right"></i>'; }else{echo ' <i class="fa fa-angle-left pull-right"></i>';} ?>
            </span>
          </a>
          <ul class="treeview-menu <?php if(isset($page) && $page==5){ echo 'class="active"'; }?> ">
           <li><a href="<?php echo base_url(); ?>admin/speakers"><i class="fa fa-circle-o"></i> Speakers</a></li>
            <li ><a href="<?php echo base_url(); ?>admin/addspeaker"><i class="fa fa-user-plus"></i>Add Speaker</a></li>
          </ul>
        </li>


      <li <?php if(isset($page) && $page==4){ echo 'class=" active"'; }?> >
        <a href="<?php echo base_url(); ?>admin/partners">
          <i class="fa fa-handshake-o"></i> <span>Partners</span>
          <span class="pull-right-container">
          <?php if(isset($page) && $page==4){ echo ' <i class="fa fa-angle-right pull-right"></i>'; }else{echo ' <i class="fa fa-angle-left pull-right"></i>';} ?>
           
          </span>
        </a>
    
      </li>
      <li class="">
        <a href="<?php echo base_url(); ?>login/logoutuser">
          <i class="fa fa-sign-out"></i> <span>   LOGOUT</span>
         
        </a>
    
      </li>




    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
