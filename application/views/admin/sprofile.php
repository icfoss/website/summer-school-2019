<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Speaker Profile
    </h1>
    <?php if( $this->session->flashdata('statusMsg')){
  echo '<div class="alert alert-danger text-center">';
  echo $this->session->flashdata('statusMsg');
  echo "</div>";
  }
  ?>
  </section>
  <?php if(isset($query1)){

  foreach ($query1 as $row1)
  {
    $sid=$row1->sp_id;
    $name=$row1->sp_name;
    $designation=$row1->sp_des;
    $org=$row1->sp_org;
    $sess=$row1->sp_session;
    $about=$row1->sp_about;
    $image=$row1->sp_image;
      }
    }
    ?>
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url();?>uploads/speaker/<?php echo $image;?>"alt="speaker profile picture">
              <h3 class="profile-username text-center"><?php echo $name;?></h3>
              <p class="text-muted text-center"><?php echo $designation;?></p>
             </div>
            <!-- /.box-body -->
          </div>
          <div class="box box-primary">
            <div class="box-header with-border">
              <!-- <h3 class="box-title">Organisation</h3> -->
            </div>
            <!-- /.box-header -->
             <div class="box-body">
               <strong><i class="fa fa-book margin-r-5"></i>Organisation</strong>

               <p class="text-muted" style="color:blue;">
               <?php echo $org;?>
               </p>
           </div>
           <div class="box-body">
             <strong><i class="fa fa-microphone"></i> Session</strong>

             <p class="text-muted" style="color:green;">
             <?php echo $sess;?>
             </p>
         </div>
           </div>
         </div>


           <div class="col-md-9">
             <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                 <li class="active"><a href="#activity" data-toggle="tab">More</a></li>

           </ul>

           <div class="tab-content">
             <div class="active tab-pane" id="activity">
               <!-- Post -->

                 <div class="user-block">

                      <div class="row">
                      <div class="col-md-12 col-sm-12 text-center" style="margin-top:10px; margin-bottom:10px;">
                       <strong>About</strong>
                       </div>

                       <div class="col-md-12 col-sm-12 " style="padding:10px;">
                         <p class="text-justify" >
                         <?php echo $about;?>
                 </p>
                 </div>
               </div>


             </div>
           </div>
    </div>
 </div>
 </div>
 </div>











    </div>
 </div>

     </section>











   </div>
</div>

    </section>
</div>
