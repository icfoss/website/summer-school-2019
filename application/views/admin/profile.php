

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <?php if( $this->session->flashdata('statusMsg')){
    echo '<div class="alert alert-danger text-center">';
    echo $this->session->flashdata('statusMsg');
    echo "</div>";
    }
    ?>
    </section>
    <?php if(isset($query1)){

foreach (  $query1 as $row1)
{
$rid=$row1->rid;
$fullname=$row1->fullname;
$name=explode(".", $fullname);
unset($name[0]);
$name=implode(".",$name);
$address=$row1->address;
$email=$row1->email;
$mob=$row1->mobile_no;
$edu=$row1->edu;
$cate=$row1->cate;
$area=$row1->area;
$reason=$row1->reason;
$photo=$row1->photo;
$profile=$row1->profile;
$proposal=$row1->proposal;
$casestudy=$row1->casestudy;
$status=$row1->status;

    }
  }
  ?>
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url(); ?>uploads/register/<?php echo $name;?>/<?php echo $photo;?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $fullname;?></h3>

              <p class="text-muted text-center"><?php echo $cate;?></p>




            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

              <p class="text-muted">
              <?php echo $edu;?>
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Address</strong>

              <p class="text-muted"><?php echo $address;?></p>

              <hr>

              <strong><i class="fa fa-envelope margin-r-5"></i> E-mail</strong>

              <p class="text-muted"><?php echo $email;?></p>

              <hr>

              <strong><i class="fa fa-mobile margin-r-5"></i> Mobile</strong>

              <p class="text-muted"><?php echo $mob;?></p>





            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">More</a></li>

              <?php if(isset($status)&&$status!=1)
              {

              ?>
              <li><a href="#settings" data-toggle="tab">Selection</a></li>
            <?php }
             ?>

            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- Post -->
                <div class="post">
                  <div class="user-block">

                       <div class="row">
                       <div class="col-md-12 col-sm-12 text-center" style="margin-top:10px; margin-bottom:10px;">
                        <strong>Area of Interest</strong>
                        </div>

                        <div class="col-md-12 col-sm-12 " style="padding:10px;">
                          <p class="text-justify" >
                          <?php echo $area;?>
                  </p>
                  </div>
                  <div class="col-md-12 col-sm-12 text-center" style="margin-top:10px; margin-bottom:10px;">
                  <strong>Reason for Choosing</strong>
                </div>
                <div class="col-md-12 col-sm-12 " style="padding:10px;">
                  <p class="text-justify">
                  <?php echo $reason;?>
                  </p>
                  </div>
                  <div class="col-md-12 col-sm-12 text-center" style="margin-top:10px; margin-bottom:20px;">
                  <strong>Download</strong>
                </div>
                  <div class="col-md-6 col-sm-6 text-center">
              <a href="<?php echo base_url(); ?>uploads/register/<?php echo $name;?>/<?php echo $profile;?>"> <button class="btn btn-info btn-block"><i class="fa fa-download" ></i> CV</button></a>
                </div>
                <div class="col-md-6 col-sm-6 text-center">
                <a href="<?php echo base_url(); ?>uploads/register/<?php echo $name;?>/<?php echo $casestudy;?>">  <button class="btn btn-info btn-block"><i class="fa fa-download"></i> IoT  CaseStudy</button></a>
                </div>
                 </div>
                </div>
              </div>
           </div>
           <div class="tab-pane" id="settings">
           <div class="col-md-12 col-sm-12 text-center" style="margin-top:10px; margin-bottom:20px;">

                  <strong>ADD TO</strong>
                </div>
              <div class="col-md-4 col-sm-4 text-center">
              <a href="<?php echo base_url(); ?>admin/selectupdate?rid=<?php echo $rid;?>&ltype=1&name=<?php echo $row1->fullname;?>&email=<?php echo $row1->email;?>"<button class="btn btn-success btn-block"> Selection List</button></a>
                </div>
                <div class="col-md-4 col-sm-4 text-center">
                 <a href="<?php echo base_url(); ?>admin/selectupdate?rid=<?php echo $rid;?>&ltype=2"<button class="btn btn-warning btn-block">Waiting List</button></a>
                </div>
                <div class="col-md-4 col-sm-4 text-center">
                 <a href="<?php echo base_url(); ?>admin/selectupdate?rid=<?php echo $rid;?>&ltype=3"<button class="btn btn-danger btn-block">Rejection List</button></a>
                </div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->



    </section>
    <!-- /.content -->
  </div>-->
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
