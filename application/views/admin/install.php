<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ICFOSS -SCHOOL |install</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition lockscreen">
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
  <a href="<?php echo base_url(); ?>"><img src="http://schools.icfoss.org/frontend/images/logo.svg"></a>
  </div>


  <!-- START LOCK SCREEN ITEM -->
  <div class="lockscreen-item text-center" style="background-color:rgba(255,255,255,0.001);" >
   
   <div class="text-center" id="install_d">
          <button id="install" type="button" class="btn  btn-success btn-lg btn-block">INSTALL</button>
     </div>
     <canvas id="division-loader" width="46" height="46"></canvas>
     <div id="installtext"><b><h4>Installing...</h4></b></div>
  </div>

 
</div>
<!-- /.center -->
<script>
var start = null;
var duration = 3000;
var boundaryIncrementer = duration / 6;

function drawDivisionLoader(timestamp) {

	// Timing Setup
	if (!start) {
		start = timestamp;
	}

	// Canvas setup
	var canvas = document.getElementById('division-loader');
	var ctx = canvas.getContext('2d');
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	// Draw inner orange circle
	ctx.lineWidth = 2.5;
	ctx.strokeStyle = '#d89747';
	ctx.beginPath();
	ctx.arc(22, 22, 10, 0, 2 * Math.PI);
	ctx.stroke();
	ctx.closePath();

	// Draw outer circle
	ctx.lineWidth = 1;
	ctx.strokeStyle = '#363537';
	ctx.beginPath();
	ctx.arc(22, 22, 20.5, 0, 2 * Math.PI);
	ctx.stroke();

	// Draw animating arcs
	ctx.lineWidth =5.5;

	// Find the remainder
	var remainder = (timestamp - start) % 3000;

	// Find out where the remainder lies within the boundaries
	if (remainder >= 0 && remainder <= (boundaryIncrementer) ) {
		// Arc 1
		ctx.strokeStyle = '#d89747';
		ctx.beginPath();
		ctx.arc(22, 22, 17, (332 * Math.PI)/180, (27 * Math.PI)/180);
		ctx.stroke();

		// Arc 2
		ctx.strokeStyle = '#363537';
		ctx.beginPath();
		ctx.arc(22, 22, 17, (32 * Math.PI)/180, (87 * Math.PI)/180);
		ctx.stroke();

		// Arc 3
		ctx.beginPath();
		ctx.arc(22, 22, 17, (92 * Math.PI)/180, (147 * Math.PI)/180);
		ctx.stroke();

		// Arc 4
		ctx.beginPath();
		ctx.arc(22, 22, 17, (152 * Math.PI)/180, (207 * Math.PI)/180);
		ctx.stroke();

		// Arc 5
		ctx.beginPath();
		ctx.arc(22, 22, 17, (212 * Math.PI)/180, (267 * Math.PI)/180);
		ctx.stroke();

		// Arc 6
		ctx.beginPath();
		ctx.arc(22, 22, 17, (272 * Math.PI)/180, (327 * Math.PI)/180);
		ctx.stroke();
	}

	if (remainder > (boundaryIncrementer) && remainder <= (boundaryIncrementer * 2) ) {
		// Arc 1
		ctx.beginPath();
		ctx.arc(22, 22, 17, (332 * Math.PI)/180, (27 * Math.PI)/180);
		ctx.stroke();

		// Arc 2
		ctx.strokeStyle = '#d89747';
		ctx.beginPath();
		ctx.arc(22, 22, 17, (32 * Math.PI)/180, (87 * Math.PI)/180);
		ctx.stroke();

		// Arc 3
		ctx.strokeStyle = '#363537';
		ctx.beginPath();
		ctx.arc(22, 22, 17, (92 * Math.PI)/180, (147 * Math.PI)/180);
		ctx.stroke();

		// Arc 4
		ctx.beginPath();
		ctx.arc(22, 22, 17, (152 * Math.PI)/180, (207 * Math.PI)/180);
		ctx.stroke();

		// Arc 5
		ctx.beginPath();
		ctx.arc(22, 22, 17, (212 * Math.PI)/180, (267 * Math.PI)/180);
		ctx.stroke();

		// Arc 6
		ctx.beginPath();
		ctx.arc(22, 22, 17, (272 * Math.PI)/180, (327 * Math.PI)/180);
		ctx.stroke();
	}

	if (remainder > (boundaryIncrementer * 2) && remainder <= (boundaryIncrementer * 3) ) {
		// Arc 1
		ctx.beginPath();
		ctx.arc(22, 22, 17, (332 * Math.PI)/180, (27 * Math.PI)/180);
		ctx.stroke();

		// Arc 2
		ctx.beginPath();
		ctx.arc(22, 22, 17, (32 * Math.PI)/180, (87 * Math.PI)/180);
		ctx.stroke();

		// Arc 3
		ctx.strokeStyle = '#d89747';
		ctx.beginPath();
		ctx.arc(22, 22, 17, (92 * Math.PI)/180, (147 * Math.PI)/180);
		ctx.stroke();

		// Arc 4
		ctx.strokeStyle = '#363537';
		ctx.beginPath();
		ctx.arc(22, 22, 17, (152 * Math.PI)/180, (207 * Math.PI)/180);
		ctx.stroke();

		// Arc 5
		ctx.beginPath();
		ctx.arc(22, 22, 17, (212 * Math.PI)/180, (267 * Math.PI)/180);
		ctx.stroke();

		// Arc 6
		ctx.beginPath();
		ctx.arc(22, 22, 17, (272 * Math.PI)/180, (327 * Math.PI)/180);
		ctx.stroke();
	}

	if (remainder > (boundaryIncrementer * 3) && remainder <= (boundaryIncrementer * 4) ) {
		// Arc 1
		ctx.beginPath();
		ctx.arc(22, 22, 17, (332 * Math.PI)/180, (27 * Math.PI)/180);
		ctx.stroke();

		// Arc 2
		ctx.beginPath();
		ctx.arc(22, 22, 17, (32 * Math.PI)/180, (87 * Math.PI)/180);
		ctx.stroke();

		// Arc 3
		ctx.beginPath();
		ctx.arc(22, 22, 17, (92 * Math.PI)/180, (147 * Math.PI)/180);
		ctx.stroke();

		// Arc 4
		ctx.strokeStyle = '#d89747';
		ctx.beginPath();
		ctx.arc(22, 22, 17, (152 * Math.PI)/180, (207 * Math.PI)/180);
		ctx.stroke();

		// Arc 5
		ctx.strokeStyle = '#363537';
		ctx.beginPath();
		ctx.arc(22, 22, 17, (212 * Math.PI)/180, (267 * Math.PI)/180);
		ctx.stroke();

		// Arc 6
		ctx.beginPath();
		ctx.arc(22, 22, 17, (272 * Math.PI)/180, (327 * Math.PI)/180);
		ctx.stroke();
	}

	if (remainder > (boundaryIncrementer * 4) && remainder <= (boundaryIncrementer * 5) ) {
		// Arc 1
		ctx.beginPath();
		ctx.arc(22, 22, 17, (332 * Math.PI)/180, (27 * Math.PI)/180);
		ctx.stroke();

		// Arc 2
		ctx.beginPath();
		ctx.arc(22, 22, 17, (32 * Math.PI)/180, (87 * Math.PI)/180);
		ctx.stroke();

		// Arc 3
		ctx.beginPath();
		ctx.arc(22, 22, 17, (92 * Math.PI)/180, (147 * Math.PI)/180);
		ctx.stroke();

		// Arc 4
		ctx.beginPath();
		ctx.arc(22, 22, 17, (152 * Math.PI)/180, (207 * Math.PI)/180);
		ctx.stroke();

		// Arc 5
		ctx.strokeStyle = '#d89747';
		ctx.beginPath();
		ctx.arc(22, 22, 17, (212 * Math.PI)/180, (267 * Math.PI)/180);
		ctx.stroke();

		// Arc 6
		ctx.strokeStyle = '#363537';
		ctx.beginPath();
		ctx.arc(22, 22, 17, (272 * Math.PI)/180, (327 * Math.PI)/180);
		ctx.stroke();
	}

	if (remainder > (boundaryIncrementer * 5) && remainder < (boundaryIncrementer * 6) ) {
		// Arc 1
		ctx.beginPath();
		ctx.arc(22, 22, 17, (332 * Math.PI)/180, (27 * Math.PI)/180);
		ctx.stroke();

		// Arc 2
		ctx.beginPath();
		ctx.arc(22, 22, 17, (32 * Math.PI)/180, (87 * Math.PI)/180);
		ctx.stroke();

		// Arc 3
		ctx.beginPath();
		ctx.arc(22, 22, 17, (92 * Math.PI)/180, (147 * Math.PI)/180);
		ctx.stroke();

		// Arc 4
		ctx.beginPath();
		ctx.arc(22, 22, 17, (152 * Math.PI)/180, (207 * Math.PI)/180);
		ctx.stroke();

		// Arc 5
		ctx.beginPath();
		ctx.arc(22, 22, 17, (212 * Math.PI)/180, (267 * Math.PI)/180);
		ctx.stroke();

		// Arc 6
		ctx.strokeStyle = '#d89747';
		ctx.beginPath();
		ctx.arc(22, 22, 17, (272 * Math.PI)/180, (327 * Math.PI)/180);
		ctx.stroke();
	}

	window.requestAnimationFrame(drawDivisionLoader);
	
}


</script>

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>backend/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>backend/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script>
$(document).ready(function(){
$("#installtext").hide();
$("#install").click(function(){
$("#install_d").empty();
$(".lockscreen-logo").css("position","relative");
$(".lockscreen-logo").css("top","75px");
$(".lockscreen-logo").css("right","5px");
$("#installtext").show();
window.requestAnimationFrame(drawDivisionLoader);

});
});

</script>

</body>
</html>
