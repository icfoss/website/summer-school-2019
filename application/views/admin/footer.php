<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 1.0.0
  </div>
  <strong>All Content On This Site Is Available Under The Creative Commons Attribution-Share A Like 4.0 Unported (CC BY-SA 3.0) License.</strong> 
</footer>
