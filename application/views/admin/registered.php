
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Registeration

                </h1>

            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Registered Candidates</h3>
                        <?php if( $this->session->flashdata('statusMsg')){
echo '<div class="alert alert-danger text-center">';
echo $this->session->flashdata('statusMsg');
echo "</div>";
}
?>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                    <div class="table-responsive">

              <table id="rejected" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Serail No:</th>
                  <th>Name</th>
                  <th>E-mail</th>
                  <th style="width: 40px">Status</th>
                  <th style="width: 40px">View</th>
                  <th style="width: 40px">Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php if(isset($query)){
                    $c=1;
                  foreach (  $query as $row)
                  {

                    ?>
                <tr>
                  <td><?php echo $c;?></td>
                  <td><?php echo $row->fullname;?></td>
                  <td>
                  <?php echo $row->email;?>
                  </td>
                  <td>
                  <?php if($row->status==0){
                   echo ' <span class=" label label-primary"> Registerd </span>';

                  }
                  else if($row->status==1){

                    echo ' <span class=" label  bg-green"> Selected </span>';

                  }
                  else if($row->status==2){

                    echo ' <span class=" label bg-yellow"> Waiting </span>';

                  }
                  else
                  {
                    echo ' <span class=" label bg-red"> Rejected </span>';
                  }
                  ?>





                    </td>
                  <td><a href="<?php echo base_url(); ?>/admin/view?rid=<?php echo $row->rid;?> " ><button class="badge bg-red">View</button></a></td>
                    <td><button class="badge bg-red"data-toggle="modal" data-target="#modal-danger">Remove</button></a></td>
                </tr>
               <?php
               $c++;
               }
              }
               ?>
                </tbody>
                <tfoot>
                <tr>
                <th>Serail No:</th>
                  <th>Name</th>
                  <th>E-mail</th>
                  <th style="width: 40px">Status</th>
                  <th style="width: 40px">View</th>
                  <th style="width: 40px">Delete</th>
                </tr>
                </tfoot>
              </table>
</div>
                    </div>
                    <!-- /.box-body -->
                    <!--<div class="box-footer">

                    </div>-->
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->

                        <!--modal--->
                        <div class="modal modal-danger fade" id="modal-danger">
                                 <div class="modal-dialog">
                                   <div class="modal-content">
                                     <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                         <span aria-hidden="true">&times;</span></button>
                                       <h4 class="modal-title">Warning!!</h4>
                                     </div>
                                     <div class="modal-body">
                                       <p>This will permenantly delete the candidate details. Do you really want to continue?&hellip;</p>
                                     </div>
                                     <div class="modal-footer">
                                       <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
                                      <a href="<?php echo base_url(); ?>/admin/rremove?rid=<?php echo $row->rid;?>" ><button type="button" class="btn btn-outline">Continue</button></a>
                                     </div>
                                   </div>

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->


        <script>
   $(document).ready(function () {

    $('#rejected').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });
</script>
