


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                   EDIT SPEAKER DETAILS
                </h1>

            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <!-- <h3 class="box-title">Partners List</h3> -->

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>

                        </div>
                    </div>
                    <?php if(isset($query)){

                      foreach ($query as $row1 ) {

                       $sname=$row1->sp_name;

                     }}?>


                    <div class="box-body">


                          <?php echo form_open_multipart('admin/sp_edit'); ?>
                        <div class="container">
                         <!-- <div>
                          <label class="control-label col-sm-3 col-md-3 text-left" for="name"><b>Name</b></label>
                          <div class="col-sm-8 col-md-8">
                          <input type="text" placeholder="Enter Name" name="spname" required>
                        </div>
                        </div>
                           <div>
                           <label class="control-label col-sm-3 col-md-3 text-left" for="des"><b>Designation</b></label>
                            <div class="col-sm-8 col-md-8">
                            <input type="text" placeholder="Enter designation" name="des" required>
                          </div>
                         </div> -->
                            <div>
                          <label class="control-label col-sm-3 col-md-3 text-left" for="id"><b>Name</b></label>
                            <div class="col-sm-8 col-md-8">
                          <input type="text" placeholder="" name="sname" value="<?php echo $sname;?>">
                          </div>

                        </div>
                          <div>
                          <label class="control-label col-sm-3 col-md-3 text-left" for="session"><b>Session Number</b></label>
                            <div class="col-sm-8 col-md-8">
                          <input type="text" placeholder="Enter session number" name="session" required>
                        </div>
                      </div>
                      <div>
                          <label class="control-label col-sm-3 col-md-3 text-left" for="about"><b>About</b></label>
                            <div class="col-sm-8 col-md-8">
                           <textarea name="about" class="form-control" rows="5" id="about"></textarea>
                        </div>
                          </div>
                       <!-- <div>
                          <label class="control-label col-sm-3 col-md-3 text-left" for="img"><b>Upload photo</b></label>
                          <div class="col-sm-8 col-md-8">
                          <input name="img" class="input-file" id="my-file" type="file" value="">
                        </div>
                        </div> -->
                           <!-- <hr> -->
                          <!-- <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p> -->

                        </div>

                        <!-- <div class="container signin">
                          <p>Already have an account? <a href="#">Sign in</a>.</p>
                        </div> -->



                      </body>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">

                      <div class="control-label col-sm-12 col-md-12 text-center">
                       <button type="submit" class="registerbtn">Submit</button>
                     </div>

                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
                  <?php echo form_close(); ?>

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <script>
   $(document).ready(function () {

    $('#waiting').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });
</script>

<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  background-color: black;
}

* {
  box-sizing: border-box;
}

/* Add padding to containers */
.container {
  padding: 20px;
  background-color: white;
  text-align:center;
}

/* Full-width input fields */
input[type=text],input[type=textarea]{
  width: 80%;
  padding: 10px;
  margin: 2px 0 12px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus{
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
/* hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
} */

/* Set a style for the submit button */
.registerbtn {
  background-color: #4CAF50;
  color: white;
  text-align:center;
  padding: 16px 20px;
  margin: 4px 2px;
  border: none;
  cursor: pointer;
  width: 25%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity: 1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}
</style>
