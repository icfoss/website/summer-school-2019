
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>/backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>/backend/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>/backend/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>/backend/dist/js/demo.js"></script>

<script src="<?php echo base_url(); ?>/backend/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
</body>
</html>
