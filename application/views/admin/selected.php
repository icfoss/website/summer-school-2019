

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                 Selected List

                </h1>

            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">   Selected list Candidates</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                    <div class="table-responsive">
              <table id="selected" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Serial No:</th>
                  <th>Name</th>
                  <th>E-mail</th>
                  <th style="width: 40px">View</th>
                  <th style="width: 40px">Remove</th>
                </tr>
                </thead>
                <tbody>
<?php if(isset($query))
{
;
$c=1;

foreach ($query as $row1 ) {

$rid=$row1->rid;




?>



                <tr>
                  <td><?php echo $c;?></td>
                  <td><?php echo $row1->fullname;?></td>
                  <td><?php echo $row1->email;?></td>
                  <td><a href="<?php echo base_url(); ?>/admin/view?rid=<?php echo $rid;?> "><button class="badge bg-red">View</button></a></td>
                    <td><a href="<?php echo base_url(); ?>/admin/listremove?l_rid=<?php echo $rid;?>"><button class="badge bg-red">Remove</button></a></td>
                </tr>

<?php
$c++;
}}
 ?>

                </tbody>
                <tfoot>
                <tr>
                <th>Serail No:</th>
                  <th>Name</th>
                  <th>E-mail</th>
                  <th style="width: 40px">View</th>
                  <th style="width: 40px">Remove</th>
                </tr>
                </tfoot>
              </table>
            </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">

                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
          </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <script>
   $(document).ready(function () {

    $('#selected').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });
</script>
