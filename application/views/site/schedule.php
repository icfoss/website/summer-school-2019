
<div class="gb-breadcrumb gb-bg white-color">
	<div class="container">
		<div class="breadcrumb-info text-center">
			<div class="page-title">
				<h1>
					<span class="before-top"></span>
					<span>Syllabus</span>
					<span class="before-bottom"></span>
        </h1>
        <h3>
            <span class="before-top"></span>
            <span>Internet of Things</span>
            <span class="before-bottom"></span>
          </h3>
			</div>
		
		</div>
	</div><!-- /.container -->
</div><!-- /.gb-breadcrumb -->

<div class="gb-schedules width-bg gb-section gb-bg text-center">
			<div class="container">
				<div class="title-section white-title">
			
				</div>
				<div class="schedule-content text-left">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#one" aria-controls="one" role="tab" data-toggle="tab"><span>Day One</span> 21 May 2019</a></li>
						<li role="presentation"><a href="#two" aria-controls="two" role="tab" data-toggle="tab"><span>Day Two</span> 22 May 2019</a></li>
						<li role="presentation"><a href="#three" aria-controls="three" role="tab" data-toggle="tab"><span>Day Three</span> 23 May 2019</a></li>
            <li role="presentation" ><a href="#four" aria-controls="four" role="tab" data-toggle="tab"><span>Day Four</span> 24 May 2019</a></li>
            <li role="presentation"><a href="#five" aria-controls="five" role="tab" data-toggle="tab"><span>Day Five</span> 25 May 2019</a></li>
            <li role="presentation"><a href="#six" aria-controls="six" role="tab" data-toggle="tab"><span>Day Six</span> 27 May 2019</a></li>
            <li role="presentation" ><a href="#seven" aria-controls="seven" role="tab" data-toggle="tab"><span>Day Seven</span> 28 May 2019</a></li>
            <li role="presentation"><a href="#eight" aria-controls="egiht" role="tab" data-toggle="tab"><span>Day Eight</span> 29 May 2019</a></li>
            <li role="presentation"><a href="#nine" aria-controls="nine" role="tab" data-toggle="tab"><span>Day Nine</span> 30 May 2019</a></li>
            <li role="presentation"><a href="#nine" aria-controls="nine" role="tab" data-toggle="tab"><span>Day Ten</span> 31 May 2019</a></li>
          </ul>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="one">
							<div class="schedule">
							<div class="row">
                <div class="col-md-12 text-center">
                  <h2>Basics of Internet and Protocols</h2>
                </div>
                <div class="col-md-8 syllabus ">

  <p>Day 1 shall introduce participants to
the fundamentals of Internet, its
history and underlying technologies.</p>
<ul>
<li>  2 Hour Talk by an Expert.</li>
<li>  2 Hour Networking Workshop.</li>
<li>  3 Hour Games and Team Building.</li>
</ul>

                </div>
                <div class="col-md-3 text-center">
<img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day1.jpg" alt="Logo">
                </div>
              <div class="col-md-12 syllabus">

<h2 >OBJECTIVES</h2>

<div >
<div class="row">
  <div class="col-md-6">
<h3>Day Goals</h3>
<p>
Immersive learning experience for participants to learn the core fundamentals of
internet. The sessions shall also include group assignments to help participants
understand the core ideas</p>
</div>
  <div class="col-md-6">
<h3>Key Points</h3>
<ul>
<li>  OSI Model</li>
<li> Web Technologies</li>
<li> IT, ICT to IoT Transition</li>
</ul>
</div>
</div>
              </div>
</div>
              <div class="col-md-12 syllabus">

<h2 >OUTCOME</h2>

<div >
<h3>Deliverables</h3>
<p>
  Ability to create, test and debug IP Network<br>
  Knowledge of OSI layer and standard Internet Protocols</br>
  Teaming Up / Get to know each other
</p>
</div>
              </div>
              </div>
							</div><!-- /.schedule -->
			</div>

						<div role="tabpanel" class="tab-pane fade" id="two">
							<div class="schedule">
                <div class="row">
                  <div class="col-md-12 text-center">
                    <h2>Introduction to Internet of Things</h2>
                  </div>
                  <div class="col-md-8 syllabus ">

    <p>Day 2 shall introduce participants
to the fundamentals of Internet of
Things , its applications , security
risks and opportunities.</p>
    <ul>
    <li>  2 Hour Talk by an Expert.</li>
    <li>  2 Hour Awareness Workshop/Talk.</li>
    <li> 2 Hour Assignments.</li>
    <li>1 Hour Brainstorming Session.</li>
    </ul>

                  </div>
                  <div class="col-md-3 text-center">
    <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day2.png" alt="Logo">
                  </div>
                <div class="col-md-12 syllabus">

    <h2 >OBJECTIVES</h2>

    <div >
    <div class="row">
    <div class="col-md-6">
    <h3>Day Goals</h3>
    <p>
      An engaging experience for participants to learn the applications, technology and
  security risks associated with IoT. The sessions shall also include group assignments
  and workshops to help participants understand the core ideas.</p>
    </div>
    <div class="col-md-6">
    <h3>Key Points</h3>
    <ul>
    <li>  Building Blocks of IoT</li>
    <li> Communication Models</li>
    <li> Risks and Security Practices</li>
    </ul>
    </div>
    </div>
                </div>
</div>
                <div class="col-md-12 syllabus">

    <h2 >OUTCOME</h2>

    <div >
    <h3>Deliverables</h3>
    <p>
      Ability to identify potential IoT use cases<br>
  Define Technology stack used in IoT
    </p>
    </div>
                </div>
                </div>
                </div><!-- /.schedule -->
							</div><!-- /.schedule -->

						<div role="tabpanel" class="tab-pane fade" id="three">
							<div class="schedule">
        <!-- -------------------------------------------------------- -->
              <div class="row">
                <div class="col-md-12 text-center">
                  <h2>Hardware for IoT</h2>
                </div>
                <div class="col-md-8 syllabus ">

          <p>Day 3 shall introduce participants
          to hardware prototyping for IoT.</p>
          <ul>
          <li> 1 Hour Talk by an Expert.</li>
          <li> 3 Hour Generic Workshop on RPi</li>
          <li> 3 Hour Generic Workshop on Arduino</li>
          </ul>

                </div>
                <div class="col-md-3 text-center">
          <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day3.jpg" alt="Logo">
                </div>
              <div class="col-md-12 syllabus">

          <h2 >OBJECTIVES</h2>

          <div >
          <div class="row">
          <div class="col-md-6">
          <h3>Day Goals</h3>
          <p>
            Hands on experience for participants to create IoT applications using Single Board
    Computers and Controller Boards. The sessions shall mostly be based on group
    assignments and workshops. More practicals than theory, in summary.</p>
          </div>
          <div class="col-md-6">
          <h3>Key Points</h3>
          <ul>
          <li>Setting Up Raspberry Pi</li>
          <li>Setting up Arduino Environment</li>
          <li>Best practices for Open Source Projects</li>
          </ul>
          </div>
          </div>
              </div>
</div>
              <div class="col-md-12 syllabus">

          <h2 >OUTCOME</h2>

          <div >
          <h3>Deliverables</h3>
          <p>
            Ability to prototype using Arduino and Raspberry Pi
  Ability to select the right hardware and software for Use Cases
  Basic Programming Skills
          </p>
          </div>

              </div>
              </div>
  <!-- -------------------------------------------------------- -->



							</div><!-- /.schedule -->
						</div>
            <div role="tabpanel" class="tab-pane fade" id="four">
              <div class="schedule">

                <div class="row">
                  <div class="col-md-12 text-center">
                    <h2>Programming for IoT</h2>
                  </div>
                  <div class="col-md-8 syllabus ">

              <p>Day 4 shall introduce participants
                   to more of Python, Shell and
                    Arduino ( C ) Programming</p>
              <ul>
              <li> 1 Hour Talk by an Expert.</li>
              <li>1 Hour Shell Workshop</li>
              <li> 3 Hour Python Workshop</li>
              <li>2 Hour Arduino Programming.</li>
              </ul>

                  </div>
                  <div class="col-md-3 text-center">
              <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day4.png" alt="Logo">
                  </div>
                <div class="col-md-12 syllabus">

              <h2 >OBJECTIVES</h2>

              <div >
              <div class="row">
              <div class="col-md-6">
              <h3>Day Goals</h3>
              <p>
                Learning experience for participants, development of API's , basic applications, and
                 Human Machine Interfaces associated with IoT. Programming samples are delivered
                 during the workshop to bootstrap.</p>
              </div>
              <div class="col-md-6">
              <h3>Key Points</h3>
              <ul>
              <li>Standard programming methods for nodes and gateways</li>
              <li>Basic Dashboards and Hardware Software Interconnects</li>
              <li>Rules Engine, Actuation and Feedback</li>
              </ul>
              </div>
              </div>
                </div>
</div>

                <div class="col-md-12 syllabus">

              <h2 >OUTCOME</h2>

              <div >
              <h3>Deliverables</h3>
              <p>
                Ability to prototype an IoT solution ( non-cloud)<br/>
                Ability to define Software Architecture for Vertical Integration<br/>
                Ability to program nodes and gateways for IoT
              </p>
              </div>
                </div>


              </div><!-- /.schedule -->
            </div>
          </div>
            <div role="tabpanel" class="tab-pane fade" id="five">
              <div class="schedule">
                <div class="row">
                  <div class="col-md-12 text-center">
                    <h2>Computing for IoT</h2>
                  </div>
                  <div class="col-md-8 syllabus ">

            <p>Day 5 shall introduce participants to the
               idea of Cloud and Fog/Edge Computing.</p>
            <ul>
            <li>1 Hour Talk by an Expert.</li>
            <li>2 Hour Workshop on Cloud Computing.</li>
            <li>2 Hour Workshop on Dashboards</li>
            <li>2 Hour Project Session on fullstack development</li>
            </ul>

                  </div>
                  <div class="col-md-3 text-center">
            <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day5.png" alt="Logo">
                  </div>
                <div class="col-md-12 syllabus">

            <h2 >OBJECTIVES</h2>

            <div >
            <div class="row">
            <div class="col-md-6">
            <h3>Day Goals</h3>
            <p>
              Industrial exposure for participants to develop applications based on distributed
               computing. The sessions shall also include group assignments and workshops to
               help participants understand the core ideas.</p>
            </div>
            <div class="col-md-6">
            <h3>Key Points</h3>
            <ul>
            <li>Fog/Edge Computing Models in Action</li>
            <li>Cloud IoT ; rules engine, dashboards and analytics</li>
            <li>Scalability and Deployability</li>
            </ul>
            </div>
            </div>
                </div>
</div>
                <div class="col-md-12 syllabus">

            <h2 >OUTCOME</h2>

            <div >
            <h3>Deliverables</h3>
            <p>
              Ability to prototype an end-end Industrial Grade IoT solution
               Design applications which are massively scalable and deployable
            </p>
            </div>
                </div>
                </div>
                </div>

              </div><!-- /.schedule -->

            <div role="tabpanel" class="tab-pane fade" id="six">
              <div class="schedule">
                <div class="row">
                  <div class="col-md-12 text-center">
                    <h2>Sensors for IoT</h2>
                  </div>
                  <div class="col-md-8 syllabus ">

            <p>Day 6 shall introduce participants
                to the world of Sensors.</p>
            <ul>
            <li>1 Hour Talk by an Expert.</li>
            <li>2 Hour Workshop on Interfaces</li>
            <li>2 Hour Workshop on μC Integration</li>
            <li>2 Hour Workshop on System Integration</li>
            </ul>

                  </div>
                  <div class="col-md-3 text-center">
            <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day6.png" alt="Logo">
                  </div>
                <div class="col-md-12 syllabus">

            <h2 >OBJECTIVES</h2>

            <div >
            <div class="row">
            <div class="col-md-6">
            <h3>Day Goals</h3>
            <p>
              An exciting day on exploring various transducers used for sensing real time
              information from environment. The sessions shall also include group assignments
              and workshops to help participants understand the core ideas.</p>
            </div>
            <div class="col-md-6">
            <h3>Key Points</h3>
            <ul>
            <li>ADC, PWM and DAC</li>
            <li>I2C, SPI , USART and CAN interfaces</li>
            <li>Design for Low Power Consumption</li>
            </ul>
            </div>
            </div>
                </div>
</div>
                <div class="col-md-12 syllabus">

            <h2 >OUTCOME</h2>

            <div >
            <h3>Deliverables</h3>
            <p>
              Ability to identify sensor IoT use cases<br/>
             Ability to integrate standard sensors to projects
            </p>
            </div>
                </div>
                </div>
                </div>




              </div><!-- /.schedule -->

            <div role="tabpanel" class="tab-pane fade" id="seven">
              <div class="schedule">

                <div class="row">
                  <div class="col-md-12 text-center">
                    <h2>Comms for IoT</h2>
                  </div>
                  <div class="col-md-8 syllabus ">

            <p>Day 7 shall introduce participants
                to LoW Power Communication
                Technologies such as LoRaWAN,
                BTLE and NB IoT</p>
            <ul>
            <li> 2 Hour Talk by an Expert.</li>
            <li>3 Hour Workshop on LoRaWAN</li>
            <li> 2 Hour Workshop on System Integration.</li>
            <li>1 Hour Brainstorming Session</li>
            </ul>

                  </div>
                  <div class="col-md-3 text-center">
            <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day7.png" alt="Logo">
                  </div>
                <div class="col-md-12 syllabus">

            <h2 >OBJECTIVES</h2>

            <div >
            <div class="row">
            <div class="col-md-6">
            <h3>Day Goals</h3>
            <p>
              Hands on experience for participants to learn LoRaWAN, System integration and
              Network architecture. The sessions shall also include group assignments and
              workshops to help participants understand the core ideas.</p>
            </div>
            <div class="col-md-6">
            <h3>Key Points</h3>
            <ul>
            <li>LoRaWAN Node Development</li>
            <li>LoRaWAN Network Architecture</li>
            <li>Communication Technologies Compared</li>
            <li>System Integration</li>
            </ul>
            </div>
            </div>
                </div>
</div>
                <div class="col-md-12 syllabus">
                  <div >
            <h2 >OUTCOME</h2>
            </div>
            <div >
            <h3>Deliverables</h3>
            <p>
              Ability to create IoT applications using LoRaWAN<br/>
             Ability to create Ultra Low Power Nodes
            </p>
            </div>
                </div>
                </div>
                </div>




              </div><!-- /.schedule -->

            <div role="tabpanel" class="tab-pane fade" id="eight">
              <div class="schedule">



                <div class="row">
                  <div class="col-md-12 text-center">
                    <h2>Hardware Prototyping</h2>
                  </div>
                  <div class="col-md-8 syllabus ">

            <p> Day 8 shall introduce participants
                to Rapid Prototyping usind 3D
                Printers, CNC Milling and PCB
                 Designing.</p>
            <ul>
            <li>1 Hour Talk on Design for Manufacturing</li>
            <li>2 Hour Workshop on CAD</li>
            <li>2 Hour Workshop on PCB</li>
            <li>2 Hour Workshop at Fab Lab</li>
            </ul>

                  </div>
                  <div class="col-md-3 text-center">
            <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day8.png" alt="Logo">
                  </div>
                <div class="col-md-12 syllabus">

            <h2 >OBJECTIVES</h2>

            <div >
            <div class="row">
            <div class="col-md-6">
            <h3>Day Goals</h3>
            <p>
              A quick learning experience for participants to start working on 3D Printers and
              Milling Machines. The sessions shall also include group assignments and workshops
             to help participants understand the core ideas.</p>
            </div>
            <div class="col-md-6">
            <h3>Key Points</h3>
            <ul>
            <li>CAD Design Fundamentals</li>
            <li>3D Printing using Ultimaker</li>
            <li>PCB Design using KiCAD</li>
            </ul>
            </div>
            </div>
                </div>
</div>
                <div class="col-md-12 syllabus">
                  <div >
            <h2 >OUTCOME</h2>
            </div>
            <div >
            <h3>Deliverables</h3>
            <p>
              Ability to build PoC Hardware <br/>
             Fundamental Know How on Design for Manufacturing
            </p>
            </div>
                </div>
                </div>
                </div>







              </div><!-- /.schedule -->

              <div role="tabpanel" class="tab-pane fade" id="nine">
                <div class="schedule">

                  <div class="row">
                    <div class="col-md-12 text-center">
                      <h2>Projects Day</h2>
                    </div>
                    <div class="col-md-8 syllabus ">

              <p>24 x 7 Makethon to Build IoT
Projects to solve Real World
Problems.</p>
              <ul>
              <li> Team Event with tight timelines.</li>
              <li> 3 Hour Generic Workshop on RPi</li>
              <li> Deliverables evaluated every Hour
and Sprint adjusted in real time.</li>
              </ul>

                    </div>
                    <div class="col-md-3 text-center">
              <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day9.png" alt="Logo">
                    </div>
                  <div class="col-md-12 syllabus">

              <h2 >AWARDS</h2>
              <p>
              Best Projects to be awarded with Certificates and Cash Prizes.</p>
              </div>




                  </div>
                  </div>
                </div><!-- /.schedule -->
              </div>


				</div><!-- /.schedule-content -->
			</div><!-- /.container -->
		</div><!-- /.gb-section -->
