<?php
if (isset($query))
{
			 foreach( $query as $row)
						{

							$con_acrgss=$row->con_acrgss;

							$config_logo1=$row->con_logo1;
							$config_logo2=$row->con_logo2;


						}
					}
					?>

		<div class="gb-breadcrumb gb-bg white-color">
				<div class="container">
					<div class="breadcrumb-info text-center">
						<div class="page-title">
							<h1>
								<span class="before-top"></span>
								<span>Register</span>
								<span class="before-bottom"></span>
							</h1>
						</div>

					</div>
				</div><!-- /.container -->
			</div><!-- /.gb-breadcrumb -->
<div class="container">
<div class="row ">
<div class="col-md-12 col-sm-12 text-center">
  <img class="img-responsive reg-img" src="<?php echo base_url(); ?>/frontend/images/<?php if(isset($config_logo1)){echo   $config_logo1;}?>" alt="Logo">
<h3>  Summer School - 2019 </h3>
</div>
<div class="col-md-12 col-sm-12 text-center">
  <p> Interested applicants are requested to provide the following particulars </p>
<div col-md-4 >  <hr style="width:20%; color:rgba(244, 149, 66, 1);  border: 2px solid rgba(244, 149, 66, 1);
  border-radius: 5px; "></div>
</div>
</div>
<div class="col-md-12 col-sm-12 text-center">
<marquee><h3 style="color:red;">Registration Closed</span></h3>
 </marquee>
</div>
<div class="row">
	<div class="col-md-12">
		<?php if(validation_errors()) { ?>
		      <div class="alert alert-danger">
		        <?php echo validation_errors(); ?>

		      </div>
				<?php } ?>

<?php if( $this->session->flashdata('statusMsg')){
echo '<div class="alert alert-danger text-center">';
echo $this->session->flashdata('statusMsg');
echo "</div>";
}
?>






	</div>
<div class="col-md-12">
<?php echo form_open_multipart('main/actionregister'); ?>
    <div class="form-group ">
      <label class="control-label col-md-3 text-center" for="titile">Title:<small class="req">*</small></label>
      <div class="col-sm-8 col-md-8">
      <select name="titile" class="form-control">
  		<option value="Ms">Ms.</option>
			<option value="Mr">Mr.</option>
			<option value="Mx">Mx.</option>

    </select>
    </div>
    </div>

    <div class="form-group ">
    <label class="control-label col-sm-3 col-md-3 text-center" for="name">Full Name:<small class="req">*</small></label>
          <div class="col-sm-8 col-md-8">
        <input name="name" type="text" class="form-control " id="name" placeholder="Enter your name:" required>
        </div>
      </div>


      <div class="form-group ">
      <label class="control-label col-sm-3 col-md-3 text-center"  for="address" >Address :<small class="req">*</small></label>
            <div class="col-sm-8 col-md-8">
        <textarea name="address" class="form-control" rows="3" id="address" required>

        </textarea>
          </div>
        </div>
        <div class="form-group ">
        <label class="control-label col-sm-3 col-md-3 text-center" for="email">Email:<small class="req">*</small></label>
              <div class="col-sm-8 col-md-8">
            <input name="email" type="email" class="form-control " id="email" placeholder="Enter your email:" required>
            </div>
          </div>
          <div class="form-group ">
          <label class="control-label col-sm-3 col-md-3 text-center" for="Phone">Mobile no:<small class="req">*</small></label>
                <div class="col-sm-8 col-md-8">
              <input name="phone" type="tel" class="form-control " id="Phone" placeholder="Enter your mobile no:" required>
              </div>
            </div>

              <div class="form-group ">
              <label class="control-label col-sm-3 col-md-3 text-center" for="education">Educational Qualification:<small class="req">*</small></label>
                    <div class="col-sm-8 col-md-8">
                  <input name="qualification" type="text" class="form-control " id="education" placeholder="Enter your Qualification:" required>
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-md-3 text-center" for="category">Category:<small class="req">*</small></label>
                  <div class="col-sm-8 col-md-8">
                  <select name="category" class="form-control" required>
              		<option value="student">Student</option>
              		<option value="researchscholar">Research Scholar </option>
                  <option value="facultymember">Faculty Members </option>
                  <option value="professionals">Professionals </option>
                </select>
                </div>
                </div>
                <div class="form-group ">
                <label class="control-label col-sm-3 col-md-3 text-center" for="area">Areas of Interest : <small class="req">*</small></label>
                      <div class="col-sm-8 col-md-8">
                  <textarea name="area" class="form-control" rows="3" id="area" required>

                  </textarea>
                    </div>
                  </div>
                  <div class="form-group ">
                  <label class="control-label col-sm-3 col-md-3 " for="moto">Reason for choosing : <small class="req">*</small></label>
                        <div class="col-sm-8 col-md-8">
                    <textarea name="moto" class="form-control" rows="3" id="moto" required>

                    </textarea>
                      </div>
                    </div>
                    <div class="form-group ">
                    <label class="control-label col-sm-3 col-md-3 " for="moto">Upload photo : <small class="req">*</small></label>
                          <div class="col-sm-8 col-md-8 ">

                            <div class="input-file-container">
                               <input name="photo" class="input-file" id="my-file" type="file" value="" required>

                             </div>

                             <span class="req">Select a file,size:600x600 pixels</span>

          </div>
                      </div>
											<div class="form-group ">
											<label class="control-label col-sm-3 col-md-3 " for="moto">Upload biodata : <small class="req">*</small></label>
														<div class="col-sm-8 col-md-8 ">

															<div class="input-file-container">
																 <input name="bio" class="input-file" id="my-file2" type="file" value="" required>

															 </div>
															  <span class="req">Select a file..(max file size 5M)</span>



						</div>
												</div>
												<div class="form-group ">
												<label class="control-label col-sm-3 col-md-3 " for="moto">Upload IoT case study : <small class="req">*</small></label>
															<div class="col-sm-8 col-md-8 ">

																<div class="input-file-container">
																	 <input name="iotstudy" class="input-file" id="my-file3" type="file" value="" required>

																 </div>
																	<span class="req">Select a file..(max file size 10M)</span>
																		<p><span class="req">Refer the following link to view  sample  IoT case studies from India by IoTPanel:<a href="<?php echo base_url(); ?>/uploads/downloads/IoT_Case_Studies_India.pdf" target="_blank"> click here to download</a></span>
</p>


							</div>
													</div>

    <div class="form-group text-center">
      <div class="col-sm-offset-2 col-sm-10">
        <input type="submit" name="Submit"   class="btn btn-default">
      </div>
    </div>
</div>
  <?php echo form_close(); ?>
</div>
<div class="row">
	<div class="col-md-12">
		<hr style="margin-top:30px;">
</div>
</div>
<script>
$(document).ready(function(){
$("form").submit(function(){
	$.toast({
	    text: "registeration in progress..", // Text that is to be shown in the toast
	    heading: 'Registeration', // Optional heading to be shown on the toast
	    icon: 'info', // Type of toast icon
	    showHideTransition: 'fade', // fade, slide or plain
	    allowToastClose: true, // Boolean value true or false
	    hideAfter: false, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
	    stack: false, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
	    position: 'mid-center', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
	    textAlign: 'center',  // Text alignment i.e. left, right or center
	    loader: true,  // Whether to show loader or not. True by default
	    loaderBg: '#9EC600',  // Background color of the toast loader




	});
});



});

</script>


</div>
