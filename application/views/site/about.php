<div class="gb-breadcrumb gb-bg white-color">
				<div class="container">
					<div class="breadcrumb-info text-center">
						<div class="page-title">
							<h1>
								<span class="before-top"></span>
								<span>ICFOSS</span>

								<span class="before-bottom"></span>
							</h1>
<ul style="list-style-type: none;">
<li>	<h5>International Center for Free and Open Source Software</h5></li>
							</ul>
						</div>

					</div>
				</div><!-- /.container -->
			</div><!-- /.gb-breadcrumb -->



      <div class ="container">



							<div class="row">
									<div class= "col-md-12 col-sm-12">

									<img class="s-img  img-responsive" src="<?php echo base_url(); ?>/frontend/images/others/icfossfront.JPG" alt="icfoss" >

									</div>


									<div class= "col-md-12 ">
							<div class="para1 ">
									<p class="text-justify">

										   	ICFOSS is the outcome  of a decade of work by Free Software  enthusiasts, advocates, developers and supporters in the state of Kerala and outside. Recognizing the strides made by the FOSS community in Kerala, and sensing a need to do more, the Government of Kerala, in 2008, had set up a Committee headed by Prof. Rahul De of IIM Bangalore to recommend what further could be done to support the cause of FOSS. Prof. De's Committee, after consulting the FOSS Community and its leaders, made several recommendations to the to the Government of Kerala. The suggestions were of which setting up of a nodal organization for supporting and extending Free Software activities in the state. But, at the same time linking with communities, developers, groups and institutions abroad.</p>
												<p class="text-justify">Given the fact software is globalized, it was suggested by the FOSS community that this organization is international in character in order to enable ease of working with FOSS communities that span continents. The Government accepted this recommendation, and began implementation by registering the International Centre for Free and Open Source Software (ICFOSS) in 2009. It began the search for a Director immediately, but the initial attempts did not yield any result. Subsequently, the Government set up a Search Committee, which identified, Mr. Satish Babu, a well-known IT professional and FOSS Advocate with considerable experience in Development & NGO sectors as well as with national & international professional societies. Mr. Satish Babu took over as the first Director of ICFOSS from 1 March 2011, and set up office at Technopark, Trivandrum. Dr. Jayasankar Prasad C took charge as Director with effect from 1 September 2015.The Governance of ICFOSS is entrusted with the Governing Body appointed by the Government of Kerala  To provide support to the Programs of ICFOSS, the Advisory Committee, consisting of a panel of experts has been also appointed. ICFOSS today carries out a number of FOSS-related activities in a model that brings together Academia, Industry, Government and the FOSS Community. Activities of ICFOSS presently includes R & D; Support to FOSS software development; FOSS Pilot Programs; technology assistance to Government programs and institutions; Local Language computing; Student FOSS activities; Internet Governance; Studies on FOSS; Exploring FOSS Certification; and Capacity Building of students to enter the FOSS Community.

			</p>

									</div>
                  </div>
							</div>

                       	</div>






		<!-- JS -->


<!-- Mirrored from html.gridbootstrap.com/eventup/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Mar 2019 05:13:45 GMT -->
