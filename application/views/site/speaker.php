
<div class="gb-breadcrumb gb-bg white-color">
	<div class="container">
		<div class="breadcrumb-info text-center">
			<div class="page-title">
				<h1>
					<span class="before-top"></span>
					<span>Speakers</span>
					<span class="before-bottom"></span>
				</h1>
			</div>
		
		</div>
	</div><!-- /.container -->
</div><!-- /.gb-breadcrumb -->

<div class="gb-speakers gb-section">
	<div class="container">
		<div class="title-section">

		</div>
		<div class="row">
		<?php if(isset($query2)){

foreach ($query2 as $row3)
{
  $sid=$row3->sp_id;
  $name=$row3->sp_name;
  $designation=$row3->sp_des;
  $org=$row3->sp_org;
  $sess=$row3->sp_session;
  $about=$row3->sp_about;
  $image=$row3->sp_image;
  ?>
<div class="col-sm-6 col-md-3">
  <div class="speaker">
    <div class="speaker-image">
    <a href="<?php echo base_url(); ?>/main/profile?sp_id=<?php echo $sid;?> " ><img style="height:150px;width:170px;" src="<?php echo base_url(); ?>/uploads/speaker/<?php echo $image;?>" alt="Image" class="img-responsive"></a>
      <!-- <ul class="speaker-social gb-list list-inline">
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
      </ul> -->
    </div>
    <div class="speaker-title">
      <h2><?php echo $name;?></h2>
      <span><?php echo $designation;?>,</span>
      <span><br/><?php echo $org;?></span>
    </div>
  </div>
</div>
<?php }} ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.gb-section -->
