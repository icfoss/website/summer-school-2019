
		<div class="gb-breadcrumb gb-bg white-color">
				<div class="container">
					<div class="breadcrumb-info text-center">
						<div class="page-title">
							<h1>
								<span class="before-top"></span>
								<span>Get In Touch</span>
								<span class="before-bottom"></span>
							</h1>
						</div>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url()?>main/">Home</a></li>
							<li class="active">Contact us</li>
						</ol>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 col-sm-12">
	<h3 class="text-center font-weight-bold contact-head"> Contact Us</h3>
	<?php if(validation_errors()) { ?>
				<div class="alert alert-danger">
					<?php echo validation_errors(); ?>

				</div>
			<?php } ?>

<?php if( $this->session->flashdata('statusMsg')){
echo '<div class="alert alert-danger text-center">';
echo $this->session->flashdata('statusMsg');
echo "</div>";
}
?>
</div>


<div class="col-md-6 col-sm-12 contact-f">

	<?php echo form_open('main/actioncontact'); ?>


					<div class="form-group">
						<input name="name" type="text" class="form-control" required="required" placeholder="Your Name">
					</div>

					<div class="form-group">
						<input name="email" type="email" class="form-control" required="required" placeholder="Your Email">
					</div>

					<div class="form-group">
						<input name="subject" type="text" class="form-control" required="required" placeholder="Subject">
					</div>

					<div class="form-group">
						<textarea name="message" class="form-control" required="required" rows="7" placeholder="Your Message"></textarea>
					</div>
						<div class="form-group">
				<input type="submit" name="submit"  value="Send Message" class="btn  pull-right" style="background-color:rgba(244, 149, 66, 1);color:#000;">
          	</div>

  <?php echo form_close(); ?>
	</div>
<section class="contact-section">
<div class="col-md-6 col-sm-12 info-column  clearfix">




						<div class="inner-column wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="content-box">

								<div class="title text-center">Visit us or call us!</div>
								<ul class="list-style-three">
									<li><span class="icon fa fa-map-marker"></span>Swatantra, Ground Floor, <br> South Pavillion  Sports Hub,  <br> Karyavattom, Thiruvananthapuram, <br> Kerala - 695581</li>
									<li><span class="icon fa fa-phone"></span>+91 73566 10110</li>
									<li><span class="icon fa fa-envelope"></span>schools@icfoss.org</li>
									<li><span class="icon fa fa-clock-o"></span>Monday to Saturday 10 AM - 6 PM , Except government holidays and 4th Saturday.</li>
								</ul>
								<!--Social Boxed-->
								<ul class="social-boxed">
									<li><a href="#"><span class="fa fa-facebook"></span></a></li>
									<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
									<li><a href="#"><span class="fa fa-vimeo"></span></a></li>
									<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
									<li><a href="#"><span class="fa fa-twitter"></span></a></li>
								</ul>
							</div>
						</div>




</div>

		<div class="row">
<div class="col-md-12 col-sm-12">
<hr  style="color:white;width:100%;height:2px;">
</div>
<div class="col-md-12" >

  <iframe style="width:100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3945.260425070077!2d76.88289481410256!3d8.570939198356221!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b05bf1ca8772919%3A0xee9fad6cf58555e9!2sGreenfield+Stadium!5e0!3m2!1sen!2sin!4v1554889766295!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>


</div>
</div>
</div>
