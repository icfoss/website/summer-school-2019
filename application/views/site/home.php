<?php

if (isset($query))
{
       foreach( $query as $row)
            {

                $con_acrgss=$row->con_acrgss;
              $config_head=$row->con_heading;
              $config_logo1=$row->con_logo1;
							$config_logo2=$row->con_logo2;
							$config_about=$row->con_about;
              $config_speaker=$row->con_speaker;
              $config_schedule=$row->con_schedule;
              $config_partners=$row->con_partners;
              $config_download=$row->con_download;
              $config_event=$row->con_event;


           }

}
  ?>


<div class="gb-banner">
	<div class="banner-info text-center">
		<div class="container">
			<h1>
				<?php
				if (isset($row->con_heading))
				{

echo   $config_head;
}
?>

			</h1>
      <p style="font-weight:700;font-size:60px;color:rgb(50, 199, 229);"> "Internet of Things"</p>
      <h2><span>Residential Program</span></h2>
			<h2><span>21 May 2019 to 31 May 2019 @ </span>ICFOSS,Sports Hub,Karyavattom</h2>
      <?php
      if(isset($con_acrgss)==true && $con_acrgss==0)

      {

        ?>
			<a href="<?php echo base_url()?>main/registration" class="btn">Register Now</a>
<?php } ?>
		</div><!-- /.container -->
	</div><!-- /.banner-info -->
</div><!-- /.gb-banner -->

<?php
if(isset($config_event)==true && $config_event==1)

{

  ?>
		<div class="upcoming-events gb-section text-center">
			<div class="container">
				<div class="title-section">
					<h1 class="color-red">
						<span class="before-top"></span>
						<span>Upcoming Events</span>
						<span class="before-bottom"></span>
					</h1>
				</div>
				<div class="event-content">
					<div class="row">
						<div class="col-sm-6 col-md-3">
							<div class="event">
								<div class="gb-time color-red">
									<p>27 <span>Jul</span></p>
								</div>
								<div class="event-info">
									<h2>Berlin, Germany</h2>
									<address><p>30217 new street road Berlin, Germany</p></address>
									<a href="#" class="btn">Get a Ticket</a>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="event">
								<div class="gb-time color-red">
									<p>12 <span>Jul</span></p>
								</div>
								<h2>Munich, Germany</h2>
								<address><p>30217 new street road Berlin, Germany</p></address>
								<a href="#" class="btn">Get a Ticket</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="event">
								<div class="gb-time color-red">
									<p>12 <span>Aug</span></p>
								</div>
								<h2>Hamburg, Germany</h2>
								<address><p>30217 new street road Berlin, Germany</p></address>
								<a href="#" class="btn">Get a Ticket</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="event">
								<div class="gb-time color-red">
									<p>09 <span>Sep</span></p>
								</div>
								<h2>Cologne, Germany</h2>
								<address><p>30217 new street road Berlin, Germany</p></address>
								<a href="#" class="btn">Get a Ticket</a>
							</div>
						</div>
					</div><!-- /.row -->
				</div>
			</div><!-- /.container -->
		</div><!-- /.upcoming-events -->


  <?php
}
?>

		<div class="gb-cta-1 gb-section gb-bg">
			<div class="container intro">
				<div class=" white-color">
          <div class="row">
            <div class="col-md-8">
					<h1 style="text-decoration:underline;">	<?php
						if (isset($row->con_heading))
						{

		echo   $config_head;
		}
		?> </h1>
					<p class="text-justify" style="font-size:20px;color:black;">  <?php
            if(isset($config_about))

            {

            echo($config_about);

   } ?></p>
 </div>
 <div class="col-md-4 text-center">
   <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/aboutiot.png" alt="Logo"></div>

   <div style="margin-top:20px;">
<hr style="color:#fff; width:2px;">
   </div>
<!--   <div class="col-md-4 text-center">
 <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/CHECK.png" alt="Logo">
</div>-->


<div class="col-md-12 ">
  <h1 style="text-decoration:underline;">Eligibility criteria</h1>

   <p class="text-justify"  style="font-size:20px;color:black;">Applicants are expected to have basic experience in OOPs, Functional Programming  and Hardware Prototyping.
Applicants are recommended to upload an IoT case study along with the application. A sample list of case studies can be found <a style="color:blue;" href="<?php echo base_url(); ?>/uploads/downloads/IoT_Case_Studies_India.pdf" target="_blank"> here </a> . </p>
 <div style="margin-top:20px;">
 <hr style="color:#fff; width:2px;">
 </div>
     <h1 style="text-decoration:underline;" >Selection criteria</h1>

     <p class="text-justify"  style="font-size:20px;color:black;">
   The submissions are initially evaluated based on the registration form to meet eligibility criteria and are then graded based on case study and relevant qualifications. If eligible candidates exceed the available seats, online exam shall be conducted.

     </p>
     <p class="text-justify"  style="font-size:20px;color:black;">
       50% of the seats are reserved for women participants.
     </p>
</div>

</div>

  <div class="col-md-8">
    <h1 style="text-decoration:underline;">Registration fees</h1>

      <div class="row">
    <div class="col-md-6">
    <p style="color:black;"><h4 style="color:black;">PG Students/Faculty :</h4></p>
    </div>
    <div class="col-md-6">
    <p  ><h4 style="color:black;" >₹7,500.00</h4></p>
    </div>
    <div class="col-md-6">
    <p ><h4 style="color:black;">Professionals  :</h4></p>
    </div>
    <div class="col-md-6">
      <p  ><h4 style="color:black;">₹15,000.00</h4></p>
    </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <p><h4 style="color:black;">Available seats :</h4></p>

        </div>
        <div class="col-md-6">
          <p ><h4 style="color:black;">30 </h4></p>


        </div>
      </div>
</div>

<div class=" col-md-4 ">
<img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/price.png" alt="Logo">
</div>
<div class="col-md-12 col-sm-12 text-center">
<h3 style="color:red;">Registration Closed..</h3>
</div>
  </div>
</div>
    <div style="margin-top:20px;">
<hr style="color:#fff; width:2px;">
    </div>
					<div class="buttons text-center">
            <?php
            if(isset($con_acrgss)==true && $con_acrgss==0)

            {

              ?>
            <a href="<?php echo base_url()?>main/registration" class="btn" style="border:1px solid black;background-color:white;">Register Now</a>
          <?php } ?>
					</div>
				</div>
			</div><!-- /.container -->
		</div><!-- /.gb-cta-1 -->

    <?php
    if(isset($config_speaker)==true && $config_speaker==1)

    {

      ?>


		<div class="gb-speakers gb-section text-center">
			<a name="speaker"></a>
			<div class="container">
				<div class="title-section">
					<h1 class="color-red">
						<span class="before-top"></span>
						<span>Speakers</span>
						<span class="before-bottom"></span>
					</h1>
				</div>
				<div class="row text-left">
        <?php if(isset($query2)){

foreach ($query3 as $row3)
{
  $sid=$row3->sp_id;
  $name=$row3->sp_name;
  $designation=$row3->sp_des;
  $org=$row3->sp_org;
  $sess=$row3->sp_session;
  $about=$row3->sp_about;
  $image=$row3->sp_image;
  ?>
<div class="col-sm-6 col-md-3">
  <div class="speaker">
    <div class="speaker-image">
    <a href="<?php echo base_url(); ?>/main/profile?sp_id=<?php echo $sid;?> " ><img style="height:150px;width:170px;" src="<?php echo base_url(); ?>/uploads/speaker/<?php echo $image;?>" alt="Image" class="img-responsive"></a>
      <!-- <ul class="speaker-social gb-list list-inline">
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
      </ul> -->
    </div>
    <div class="speaker-title">
      <h2><?php echo $name;?></h2>
      <span><?php echo $designation;?>,</span>
      <span><br/><?php echo $org;?></span>
    </div>
  </div>
</div>
<?php }} ?>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.gb-section -->
    <?php }
    ?>
    <?php
    if(isset($config_schedule)==true && $config_schedule==1)

    {

      ?>

		<div class="gb-schedules width-bg gb-section gb-bg text-center">
			<div class="container">
				<div class="title-section white-title">
					<h1>
						<span class="before-top"></span>
						<span>Syllabus</span>
						<span class="before-bottom"></span>
					</h1>
          <h3>
            <span class="before-top"></span>
            <span>Internet of Things</span>
            <span class="before-bottom"></span>
          </h3>
				</div>
				<div class="schedule-content text-left">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#one" aria-controls="one" role="tab" data-toggle="tab"><span>Day One</span> 21 May 2019</a></li>
						<li role="presentation"><a href="#two" aria-controls="two" role="tab" data-toggle="tab"><span>Day Two</span> 22 May 2019</a></li>
						<li role="presentation"><a href="#three" aria-controls="three" role="tab" data-toggle="tab"><span>Day Three</span> 23 May 2019</a></li>
            <li role="presentation" ><a href="#four" aria-controls="four" role="tab" data-toggle="tab"><span>Day Four</span> 24 May 2019</a></li>
            <li role="presentation"><a href="#five" aria-controls="five" role="tab" data-toggle="tab"><span>Day Five</span> 25 May 2019</a></li>
            <li role="presentation"><a href="#six" aria-controls="six" role="tab" data-toggle="tab"><span>Day Six</span> 27 May 2019</a></li>
            <li role="presentation" ><a href="#seven" aria-controls="seven" role="tab" data-toggle="tab"><span>Day Seven</span> 28 May 2019</a></li>
            <li role="presentation"><a href="#eight" aria-controls="egiht" role="tab" data-toggle="tab"><span>Day Eight</span> 29 May 2019</a></li>
            <li role="presentation"><a href="#nine" aria-controls="nine" role="tab" data-toggle="tab"><span>Day Nine</span> 30 May 2019</a></li>
            <li role="presentation"><a href="#nine" aria-controls="nine" role="tab" data-toggle="tab"><span>Day Ten</span> 31 May 2019</a></li>
          </ul>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="one">
							<div class="schedule">
							<div class="row">
                <div class="col-md-12 text-center">
                  <h2>Basics of Internet and Protocols</h2>
                </div>
                <div class="col-md-8 syllabus ">

  <p>Day 1 shall introduce participants to
the fundamentals of Internet, its
history and underlying technologies.</p>
<ul>
<li>  2 Hour Talk by an Expert.</li>
<li>  2 Hour Networking Workshop.</li>
<li>  3 Hour Games and Team Building.</li>
</ul>

                </div>
                <div class="col-md-3 text-center">
<img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day1.jpg" alt="Logo">
                </div>
              <div class="col-md-12 syllabus">

<h2 >OBJECTIVES</h2>

<div >
<div class="row">
  <div class="col-md-6">
<h3>Day Goals</h3>
<p>
Immersive learning experience for participants to learn the core fundamentals of
internet. The sessions shall also include group assignments to help participants
understand the core ideas</p>
</div>
  <div class="col-md-6">
<h3>Key Points</h3>
<ul>
<li>  OSI Model</li>
<li> Web Technologies</li>
<li> IT, ICT to IoT Transition</li>
</ul>
</div>
</div>
              </div>
</div>
              <div class="col-md-12 syllabus">

<h2 >OUTCOME</h2>

<div >
<h3>Deliverables</h3>
<p>
  Ability to create, test and debug IP Network<br>
  Knowledge of OSI layer and standard Internet Protocols</br>
  Teaming Up / Get to know each other
</p>
</div>
              </div>
              </div>
							</div><!-- /.schedule -->
			</div>

						<div role="tabpanel" class="tab-pane fade" id="two">
							<div class="schedule">
                <div class="row">
                  <div class="col-md-12 text-center">
                    <h2>Introduction to Internet of Things</h2>
                  </div>
                  <div class="col-md-8 syllabus ">

    <p>Day 2 shall introduce participants
to the fundamentals of Internet of
Things , its applications , security
risks and opportunities.</p>
    <ul>
    <li>  2 Hour Talk by an Expert.</li>
    <li>  2 Hour Awareness Workshop/Talk.</li>
    <li> 2 Hour Assignments.</li>
    <li>1 Hour Brainstorming Session.</li>
    </ul>

                  </div>
                  <div class="col-md-3 text-center">
    <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day2.png" alt="Logo">
                  </div>
                <div class="col-md-12 syllabus">

    <h2 >OBJECTIVES</h2>

    <div >
    <div class="row">
    <div class="col-md-6">
    <h3>Day Goals</h3>
    <p>
      An engaging experience for participants to learn the applications, technology and
  security risks associated with IoT. The sessions shall also include group assignments
  and workshops to help participants understand the core ideas.</p>
    </div>
    <div class="col-md-6">
    <h3>Key Points</h3>
    <ul>
    <li>  Building Blocks of IoT</li>
    <li> Communication Models</li>
    <li> Risks and Security Practices</li>
    </ul>
    </div>
    </div>
                </div>
</div>
                <div class="col-md-12 syllabus">

    <h2 >OUTCOME</h2>

    <div >
    <h3>Deliverables</h3>
    <p>
      Ability to identify potential IoT use cases<br>
  Define Technology stack used in IoT
    </p>
    </div>
                </div>
                </div>
                </div><!-- /.schedule -->
							</div><!-- /.schedule -->

						<div role="tabpanel" class="tab-pane fade" id="three">
							<div class="schedule">
        <!-- -------------------------------------------------------- -->
              <div class="row">
                <div class="col-md-12 text-center">
                  <h2>Hardware for IoT</h2>
                </div>
                <div class="col-md-8 syllabus ">

          <p>Day 3 shall introduce participants
          to hardware prototyping for IoT.</p>
          <ul>
          <li> 1 Hour Talk by an Expert.</li>
          <li> 3 Hour Generic Workshop on RPi</li>
          <li> 3 Hour Generic Workshop on Arduino</li>
          </ul>

                </div>
                <div class="col-md-3 text-center">
          <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day3.jpg" alt="Logo">
                </div>
              <div class="col-md-12 syllabus">

          <h2 >OBJECTIVES</h2>

          <div >
          <div class="row">
          <div class="col-md-6">
          <h3>Day Goals</h3>
          <p>
            Hands on experience for participants to create IoT applications using Single Board
    Computers and Controller Boards. The sessions shall mostly be based on group
    assignments and workshops. More practicals than theory, in summary.</p>
          </div>
          <div class="col-md-6">
          <h3>Key Points</h3>
          <ul>
          <li>Setting Up Raspberry Pi</li>
          <li>Setting up Arduino Environment</li>
          <li>Best practices for Open Source Projects</li>
          </ul>
          </div>
          </div>
              </div>
</div>
              <div class="col-md-12 syllabus">

          <h2 >OUTCOME</h2>

          <div >
          <h3>Deliverables</h3>
          <p>
            Ability to prototype using Arduino and Raspberry Pi
  Ability to select the right hardware and software for Use Cases
  Basic Programming Skills
          </p>
          </div>

              </div>
              </div>
  <!-- -------------------------------------------------------- -->



							</div><!-- /.schedule -->
						</div>
            <div role="tabpanel" class="tab-pane fade" id="four">
              <div class="schedule">

                <div class="row">
                  <div class="col-md-12 text-center">
                    <h2>Programming for IoT</h2>
                  </div>
                  <div class="col-md-8 syllabus ">

              <p>Day 4 shall introduce participants
                   to more of Python, Shell and
                    Arduino ( C ) Programming</p>
              <ul>
              <li> 1 Hour Talk by an Expert.</li>
              <li>1 Hour Shell Workshop</li>
              <li> 3 Hour Python Workshop</li>
              <li>2 Hour Arduino Programming.</li>
              </ul>

                  </div>
                  <div class="col-md-3 text-center">
              <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day4.png" alt="Logo">
                  </div>
                <div class="col-md-12 syllabus">

              <h2 >OBJECTIVES</h2>

              <div >
              <div class="row">
              <div class="col-md-6">
              <h3>Day Goals</h3>
              <p>
                Learning experience for participants, development of API's , basic applications, and
                 Human Machine Interfaces associated with IoT. Programming samples are delivered
                 during the workshop to bootstrap.</p>
              </div>
              <div class="col-md-6">
              <h3>Key Points</h3>
              <ul>
              <li>Standard programming methods for nodes and gateways</li>
              <li>Basic Dashboards and Hardware Software Interconnects</li>
              <li>Rules Engine, Actuation and Feedback</li>
              </ul>
              </div>
              </div>
                </div>
</div>

                <div class="col-md-12 syllabus">

              <h2 >OUTCOME</h2>

              <div >
              <h3>Deliverables</h3>
              <p>
                Ability to prototype an IoT solution ( non-cloud)<br/>
                Ability to define Software Architecture for Vertical Integration<br/>
                Ability to program nodes and gateways for IoT
              </p>
              </div>
                </div>


              </div><!-- /.schedule -->
            </div>
          </div>
            <div role="tabpanel" class="tab-pane fade" id="five">
              <div class="schedule">
                <div class="row">
                  <div class="col-md-12 text-center">
                    <h2>Computing for IoT</h2>
                  </div>
                  <div class="col-md-8 syllabus ">

            <p>Day 5 shall introduce participants to the
               idea of Cloud and Fog/Edge Computing.</p>
            <ul>
            <li>1 Hour Talk by an Expert.</li>
            <li>2 Hour Workshop on Cloud Computing.</li>
            <li>2 Hour Workshop on Dashboards</li>
            <li>2 Hour Project Session on fullstack development</li>
            </ul>

                  </div>
                  <div class="col-md-3 text-center">
            <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day5.png" alt="Logo">
                  </div>
                <div class="col-md-12 syllabus">

            <h2 >OBJECTIVES</h2>

            <div >
            <div class="row">
            <div class="col-md-6">
            <h3>Day Goals</h3>
            <p>
              Industrial exposure for participants to develop applications based on distributed
               computing. The sessions shall also include group assignments and workshops to
               help participants understand the core ideas.</p>
            </div>
            <div class="col-md-6">
            <h3>Key Points</h3>
            <ul>
            <li>Fog/Edge Computing Models in Action</li>
            <li>Cloud IoT ; rules engine, dashboards and analytics</li>
            <li>Scalability and Deployability</li>
            </ul>
            </div>
            </div>
                </div>
</div>
                <div class="col-md-12 syllabus">

            <h2 >OUTCOME</h2>

            <div >
            <h3>Deliverables</h3>
            <p>
              Ability to prototype an end-end Industrial Grade IoT solution
               Design applications which are massively scalable and deployable
            </p>
            </div>
                </div>
                </div>
                </div>

              </div><!-- /.schedule -->

            <div role="tabpanel" class="tab-pane fade" id="six">
              <div class="schedule">
                <div class="row">
                  <div class="col-md-12 text-center">
                    <h2>Sensors for IoT</h2>
                  </div>
                  <div class="col-md-8 syllabus ">

            <p>Day 6 shall introduce participants
                to the world of Sensors.</p>
            <ul>
            <li>1 Hour Talk by an Expert.</li>
            <li>2 Hour Workshop on Interfaces</li>
            <li>2 Hour Workshop on μC Integration</li>
            <li>2 Hour Workshop on System Integration</li>
            </ul>

                  </div>
                  <div class="col-md-3 text-center">
            <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day6.png" alt="Logo">
                  </div>
                <div class="col-md-12 syllabus">

            <h2 >OBJECTIVES</h2>

            <div >
            <div class="row">
            <div class="col-md-6">
            <h3>Day Goals</h3>
            <p>
              An exciting day on exploring various transducers used for sensing real time
              information from environment. The sessions shall also include group assignments
              and workshops to help participants understand the core ideas.</p>
            </div>
            <div class="col-md-6">
            <h3>Key Points</h3>
            <ul>
            <li>ADC, PWM and DAC</li>
            <li>I2C, SPI , USART and CAN interfaces</li>
            <li>Design for Low Power Consumption</li>
            </ul>
            </div>
            </div>
                </div>
</div>
                <div class="col-md-12 syllabus">

            <h2 >OUTCOME</h2>

            <div >
            <h3>Deliverables</h3>
            <p>
              Ability to identify sensor IoT use cases<br/>
             Ability to integrate standard sensors to projects
            </p>
            </div>
                </div>
                </div>
                </div>




              </div><!-- /.schedule -->

            <div role="tabpanel" class="tab-pane fade" id="seven">
              <div class="schedule">

                <div class="row">
                  <div class="col-md-12 text-center">
                    <h2>Comms for IoT</h2>
                  </div>
                  <div class="col-md-8 syllabus ">

            <p>Day 7 shall introduce participants
                to LoW Power Communication
                Technologies such as LoRaWAN,
                BTLE and NB IoT</p>
            <ul>
            <li> 2 Hour Talk by an Expert.</li>
            <li>3 Hour Workshop on LoRaWAN</li>
            <li> 2 Hour Workshop on System Integration.</li>
            <li>1 Hour Brainstorming Session</li>
            </ul>

                  </div>
                  <div class="col-md-3 text-center">
            <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day7.png" alt="Logo">
                  </div>
                <div class="col-md-12 syllabus">

            <h2 >OBJECTIVES</h2>

            <div >
            <div class="row">
            <div class="col-md-6">
            <h3>Day Goals</h3>
            <p>
              Hands on experience for participants to learn LoRaWAN, System integration and
              Network architecture. The sessions shall also include group assignments and
              workshops to help participants understand the core ideas.</p>
            </div>
            <div class="col-md-6">
            <h3>Key Points</h3>
            <ul>
            <li>LoRaWAN Node Development</li>
            <li>LoRaWAN Network Architecture</li>
            <li>Communication Technologies Compared</li>
            <li>System Integration</li>
            </ul>
            </div>
            </div>
                </div>
</div>
                <div class="col-md-12 syllabus">
                  <div >
            <h2 >OUTCOME</h2>
            </div>
            <div >
            <h3>Deliverables</h3>
            <p>
              Ability to create IoT applications using LoRaWAN<br/>
             Ability to create Ultra Low Power Nodes
            </p>
            </div>
                </div>
                </div>
                </div>




              </div><!-- /.schedule -->

            <div role="tabpanel" class="tab-pane fade" id="eight">
              <div class="schedule">



                <div class="row">
                  <div class="col-md-12 text-center">
                    <h2>Hardware Prototyping</h2>
                  </div>
                  <div class="col-md-8 syllabus ">

            <p> Day 8 shall introduce participants
                to Rapid Prototyping usind 3D
                Printers, CNC Milling and PCB
                 Designing.</p>
            <ul>
            <li>1 Hour Talk on Design for Manufacturing</li>
            <li>2 Hour Workshop on CAD</li>
            <li>2 Hour Workshop on PCB</li>
            <li>2 Hour Workshop at Fab Lab</li>
            </ul>

                  </div>
                  <div class="col-md-3 text-center">
            <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day8.png" alt="Logo">
                  </div>
                <div class="col-md-12 syllabus">

            <h2 >OBJECTIVES</h2>

            <div >
            <div class="row">
            <div class="col-md-6">
            <h3>Day Goals</h3>
            <p>
              A quick learning experience for participants to start working on 3D Printers and
              Milling Machines. The sessions shall also include group assignments and workshops
             to help participants understand the core ideas.</p>
            </div>
            <div class="col-md-6">
            <h3>Key Points</h3>
            <ul>
            <li>CAD Design Fundamentals</li>
            <li>3D Printing using Ultimaker</li>
            <li>PCB Design using KiCAD</li>
            </ul>
            </div>
            </div>
                </div>
</div>
                <div class="col-md-12 syllabus">
                  <div >
            <h2 >OUTCOME</h2>
            </div>
            <div >
            <h3>Deliverables</h3>
            <p>
              Ability to build PoC Hardware <br/>
             Fundamental Know How on Design for Manufacturing
            </p>
            </div>
                </div>
                </div>
                </div>







              </div><!-- /.schedule -->

              <div role="tabpanel" class="tab-pane fade" id="nine">
                <div class="schedule">

                  <div class="row">
                    <div class="col-md-12 text-center">
                      <h2>Projects Day</h2>
                    </div>
                    <div class="col-md-8 syllabus ">

              <p>24 x 7 Makethon to Build IoT
Projects to solve Real World
Problems.</p>
              <ul>
              <li> Team Event with tight timelines.</li>
              <li> All Day Event with mentor support</li>
              <li> Deliverables evaluated every Hour
and Sprint adjusted in real time.</li>
              </ul>

                    </div>
                    <div class="col-md-3 text-center">
              <img class="img-responsive" src="<?php echo base_url(); ?>/frontend/images/day9.png" alt="Logo">
                    </div>
                  <div class="col-md-12 syllabus">

              <h2 >AWARDS</h2>
              <p>
              Best Projects to be awarded with Certificates and Cash Prizes.</p>
              </div>




                  </div>
                  </div>
                </div><!-- /.schedule -->
              </div>


				</div><!-- /.schedule-content -->
			</div><!-- /.container -->
		</div><!-- /.gb-section -->

  <?php }
  ?>








<?php
if(isset($config_partners)==true && $config_partners==1)

{

  ?>
		<div class="gb-brand gb-section gb-bg white-color">
			<div class="container">
				<div class="title-section white-title">
					<h1>
						<span class="before-top"></span>
						<span>Partners</span>
						<span class="before-bottom"></span>
					</h1>
				</div>
				<div class="brand-slider text-center">
        <?php
        if (isset($query2))
        {
               foreach( $query2 as $row2)
                    {
?>
					<div class="brand ">
						<img src="<?php echo base_url(); ?>/uploads/brand/<?php echo $row2->pt_image;?>" alt="<?php echo $row2->pt_name;?>" class="img-responsive">
					</div>
          <?php
                    }}
                    ?>
				</div>
			</div><!-- /.container -->
		</div><!-- /.gb-brand  -->
<?php } ?>
  <!-- map -->
 <div class="col-md-12 col-sm-12 contact-info">
<hr  style="color:white;width:100%;height:10px;">
</div>                               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3945.260425070077!2d76.88289481410256!3d8.570939198356221!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b05bf1ca8772919%3A0xee9fad6cf58555e9!2sGreenfield+Stadium!5e0!3m2!1sen!2sin!4v1554889766295!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

		<div class="gb-contact text-center">


				<div class="event">
					<div class="gb-time color-red">
						<p>21-31 <span>May 2019</span></p>
					</div>
          <h2>ICFOSS,Swatantra</H2>

					<address><p> Ground Floor,South Pavillion ,Sports Hub,Karyavattom, Thiruvananthapuram</p></address>
          <?php
          if(isset($con_acrgss)==true && $con_acrgss==0)

          {

            ?>
    			<a href="<?php echo base_url()?>main/registration" class="btn">Register Now</a>
    <?php } ?>
				</div>

			</div>
		</div><!-- /.gb-contact -->

  </div>
