<!DOCTYPE html>
<html lang="en">

<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="author" content="Theme Region">
		<meta name="description" content="">

		<title><?php echo $head_name;?></title>

		<!-- CSS -->
		<script src="<?php echo base_url(); ?>frontend/js/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>frontend/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/bootstrap.min.css" >
		<link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/magnific-popup.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/animate.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/slick.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/main.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/responsive.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/jquery.toast.min.css">

		<!-- Fonts -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet">

	    <!-- icons -->
	    <link rel="icon" href="<?php echo base_url(); ?>frontend/images/ico/favicon.ico">
	    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>frontend/images/ico/apple-touch-icon-precomposed.html">
	    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>frontend/images/ico/apple-touch-icon-114-precomposed.html">
	    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>frontend/images/ico/apple-touch-icon-72-precomposed.html">
	    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>frontend/images/ico/apple-touch-icon-57-precomposed.html">

	</head>
