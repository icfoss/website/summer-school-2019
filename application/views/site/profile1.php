
		<div class="gb-breadcrumb gb-bg white-color">
			<div class="container">
				<div class="breadcrumb-info text-center">
					<div class="page-title">
						<h1>
							<span class="before-top"></span>
							<span>Speakers</span>
							<span class="before-bottom"></span>
						</h1>
					</div>
					<ol class="breadcrumb">
						<li><a href="index-2.html">Home</a></li>
						<li class="active">Speakers</li>
					</ol>
				</div>
			</div><!-- /.container -->
		</div><!-- /.gb-breadcrumb -->


<!-- Mirrored from html.gridbootstrap.com/eventup/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Mar 2019 05:13:45 GMT -->

                     <div class ="container"
											<div class="row">
											<div class="col-md-4 text-center">

                        <img class="s-img img-responsive rounded " src="images/others/author.png" alt="speaker-foto" class="img-fluid speaker-images">
                        <div class="s-name" >
												Dr. Madhukar Gupta, IAS
											</div>


											</div>
											<div class="col-md-8">
												<div class="s-detail">
											<p>	Designation: Vice President of WFEO </p>
                      <p>  Organization: World Federation of Engineering Organizations (WFEO)</p>
											</div>
												<div class="s-paragraph">
												 <p class="text-justify">Madhukar Gupta, Ph.D is an Electrical Engineer, MBA and law graduate. He was selected as a Mason Fellow by Harvard for a Master’s in Public Administration in 2006. The Government of India sent him for a Master’s in Public Policy at Maxwell, Syracuse. He has been awarded a PhD in Economics in 2010. He is currently the Additional Secretary, Ministry of Public Enterprises and Heavy Industries, Government of India.</p>
                         <p class="text-justify">Madhukar Gupta focused on Public Policy in US and China with experts on economic growth, public sector management, infrastructure, corporate finance and development at Harvard, MIT, Syracuse, Maryland, Cornell and Fletcher School. He has won many national awards from NPC and NABARD. Indian and international Universities have hosted him as a visiting faculty and scholar, including BITS where he has been adjunct faculty for more than 20 years. He has presented professional papers in nearly three dozen countries.</p>

											 </div>
											 <div class="s-session">
												 Session:WFEO Session
											 </div>
											</div>

									  	</div>





										 	</div>






		<!-- JS -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/countdown.js"></script>
		<script src="js/slick.min.js"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
		<script src="js/gmaps.min.js"></script>
		<script src="js/magnific-popup.min.js"></script>
		<script src="js/main.js"></script>


<!-- Mirrored from html.gridbootstrap.com/eventup/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Mar 2019 05:13:45
