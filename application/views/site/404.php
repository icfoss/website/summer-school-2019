

		<div class="gb-breadcrumb gb-bg white-color">
			<div class="container">
				<div class="breadcrumb-info text-center">
					<div class="page-title">
						<h1>
							<span class="before-top"></span>
							<span>404 - Nothing to see here</span>
							<span class="before-bottom"></span>
						</h1>
					</div>
				</div>
			</div><!-- /.container -->
		</div><!-- /.gb-breadcrumb -->

		<div class="gb-not-found text-center gb-section">
			<div class="container">
				<div class="not-found-content">
					<h1 class="color-red">404</h1>
					<h2>We are sorry ! The page you are looking for, does not exist</h2>
					<a href="<?php echo base_url(); ?>" class="btn btn-bg">Back To Home</a>
				</div>
			</div><!-- /.container -->
		</div><!-- /.gb-not-found -->
