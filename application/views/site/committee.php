
<div class="gb-breadcrumb gb-bg white-color">
				<div class="container">
					<div class="breadcrumb-info text-center">
						<div class="page-title">
							<h1>
								<span class="before-top"></span>
								<span>Summer School 2019-Committee</span>

								<span class="before-bottom"></span>
							</h1>

						</div>

					</div>
				</div><!-- /.container -->
			</div><!-- /.gb-breadcrumb -->







      <div class ="container">



							<div class="row">
								<div class=" commit col-md-6 col-sm-6 ">
								  <p>Programme Committee Members</p>
									<div class="posit">
								  <ol>
								<li>Dr. Jayasankar Prasad C, Patron</li>
								<li>  Mr. R. Srinivasan, Chair</li>
								 <li>Dr. Rajeev R.R., Co-Chair</li>
								 <li>Mr. Sreekumar B, Member</li>
								  <li>Mr. Jaidev G, Member</li>
								  <li>Mr. Sampath Kumar R, Member</li>
								</ol>
								</div>
							</div>


																<div class="commit  col-md-6 col-sm-6">
																<p>Organizing Committee Members</p>
		<div class="posit">
				<ol>
 <li>Mr. Akshai M, Chair</li>
 <li> Ms. Jincy Baby, Co-Chair</li>
 <li> Mr. Dinesh Lal D.L, Member</li>
  <li> Mr. Uma Mahesh, Member</li>
 <li> Ms. Hasiya Noohu, Member</li>
 <li> Mr. Abhilash C, Member</li>
 <li> Ms. Niji G.S - Member</li>
 <li> Ms. Anupa Ann Joseph, Member</li>
 <li> Ms. Keerthana Ashok - Member</li>
 <li> Mr. Shafeek , Member</li>
 <li> Mr. Vaishak Anand, Member</li>
 <li> Mr. Akhil S.G, Member</li>
  <li> Mr. Ajmi A.S, Member</li>
 <li>Mr. Arun Chandran, Member</li>
 <li> Ms. Greeshma Rajan, Member</li>
  <li>Mr. Sudeesh V.S, Member</li>
</ol>
																</div>
															</div>
              </div>

				</div>





		<!-- JS -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/countdown.js"></script>
		<script src="js/slick.min.js"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
		<script src="js/gmaps.min.js"></script>
		<script src="js/magnific-popup.min.js"></script>
		<script src="js/main.js"></script>


<!-- Mirrored from html.gridbootstrap.com/eventup/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Mar 2019 05:13:45 GMT -->
