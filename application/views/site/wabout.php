<div class="gb-breadcrumb gb-bg white-color">
				<div class="container">
					<div class="breadcrumb-info text-center">
						<div class="page-title">
							<h1>
								<span class="before-top"></span>
								<span>Winter School For Women</span>

								<span class="before-bottom"></span>
							</h1>
							<ol>
							<!--	<li>	<h5>International Center for Fr ee and Open Source Software</h5></li>-->
							</ol>
						</div>

					</div>
				</div><!-- /.container -->
			</div><!-- /.gb-breadcrumb -->


  <div class = "container">

							<div class="row">
									<div class= "col-md-12 text-center">

									<img class="s-img img-fluid img-responsive img-thumbnail" src="<?php echo base_url(); ?>/frontend/images/others/pgm.png" alt="winter-school" >

									</div>
   <div class= "col-md-12 ">
   	<div class="para1 ">
									<p class="text-justify">

									 The Winter School for Women 2019, on “Natural Language Processing (NLP) &
 Machine Learning (ML)”, organized by International Centre for Free and Open Source
 Software (ICFOSS) in collaboration with central institute of Indian Languages (CIIL) and co-
 sponsored by Kerala IT Mission, was held from Jan 15 th to 25 th 2019 at Club House,
 Technopark.
 The school convened women who were Post Graduate students, Research
scholars, Faculty and Professionals from industry. The aim of the school was to improve
women participation in FOSS communities as well as mould the individuals in the field of
Natural Language Processing.

    The invited talks and hands-on sessions provided an
			 opportunity for the participants to acquire individual attention of mentors and to develop
			 network between themselves. The winter school encouraged the participants to come up with
			 the best and brightest ideas in various areas of natural language processing and machine
			 learning.
</p>

									</div>
                  </div>
									<div class="col-md-12 col-sm -12">
										<h3>Participants</h3>
										<p class="para1 text-justify">
											A total of 42 women participants attended the school, with 30 registered participants and 12
women researchers from ICFOSS as internal participants.
										</p>
									</div>
									<div class="col-md-12 col-sm-12">
									<h3>	Event Outcome</h3>
 <p class="para1 text-justify" >The event involved the women community focussing on the academic and research
 development in the area of language processing. The participants were keen on presenting the
 project proposals and they positively took the suggestion to keep in contact with each other to
 develop the projects they came up with.</p>
 <p class="para1 text-justify">Detailed report can be found  <a style="color:blue;" href="<?php echo base_url(); ?>/uploads/downloads/WS_report .pdf"> here </a></p>

 </details>
									</div>
							</div>


				</div>





		<!-- JS -->



<!-- Mirrored from html.gridbootstrap.com/eventup/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Mar 2019 05:13:45 GMT -->
