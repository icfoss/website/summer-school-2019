<div class="gb-breadcrumb gb-bg white-color">
				<div class="container">
					<div class="breadcrumb-info text-center">
						<div class="page-title">
							<h1>
								<span class="before-top"></span>
								<span>Summer School</span>

								<span class="before-bottom"></span>
							</h1>
							<ol>
							<!--	<li>	<h5>International Center for Free and Open Source Software</h5></li>-->
							</ol>
						</div>

					</div>
				</div><!-- /.container -->
			</div><!-- /.gb-breadcrumb -->






      <div class ="container">

							<div class="row">
									<div class= "col-md-4 text-center">

									<img class="s-img img-fluid" src="<?php echo base_url(); ?>/frontend/images/others/Apps-for-IoT.png" alt="summer-school" >

									</div>

                <br/><br/>
								<br/>
									<div class= "col-md-8 ">
						         	<div class="para1 ">
									<p class="text-justify">Summer School is a Certification Program  designed by ICFOSS to empower
technologists with Industrial Knowledge in the particular domain through hands on trainings and workshops. Summer School
sessions are Project Oriented i.e. the content is covered practically with concepts of theoretical knowledge. The camp for this year shall be based on the
theme Internet of Things. The internet of things (IoT) is a computing concept that describes the idea of everyday physical objects being
connected to the internet and being able to identify themselves to other devices.</p>
									</div>
                  </div>
									</div>
							<div class="row">
									<div class="paragraph1">
									<p class="text-justify">The Internet of Things domain will encompass an extremely wide range of
technologies, from stateless to stateful, from extremely constrained to unconstrained, from hard real time to soft
real time.The summer camp shall feature Sessions on various verticals of IoT. Talks and workshops shall go hand in hand to
deliver an immersive learning experience and undertake a project at the end of the camp..</p>
                  <!-- <p class="text-justify">Madhukar Gupta focused on Public Policy in US and China with experts on economic growth, public sector management, infrastructure, corporate finance and development at Harvard, MIT, Syracuse, Maryland, Cornell and Fletcher School. He has won many national awards from NPC and NABARD. Indian and international Universities have hosted him as a visiting faculty and scholar, including BITS where he has been adjunct faculty for more than 20 years. He has presented professional papers in nearly three dozen countries.</p> -->
								  </div>
									</div>

				</div>





		<!-- JS -->



<!-- Mirrored from html.gridbootstrap.com/eventup/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Mar 2019 05:13:45 GMT -->
