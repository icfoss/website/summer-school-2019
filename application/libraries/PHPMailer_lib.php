<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class PHPMailer_lib {
    public function __construct(){
        log_message('Debug', 'PHPMailer class is loaded.');
    }

    public function load(){


        require_once APPPATH.'third_party/PHPmailer/PHPMailer.php';
        require_once APPPATH.'third_party/PHPmailer/SMTP.php';
        require_once APPPATH.'third_party/PHPmailer/Exception.php';

        $mail = new PHPMailer(true);
        return $mail;
    }
}
